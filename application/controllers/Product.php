<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
    function hello()
    {
        echo "it works";
    }
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
	}

	public function whatsapp()
	{
		redirect('https://api.whatsapp.com/send?phone=6281259929797&amp;text=Halo%20gan,%20Saya%20mau%20order');
	}
	public function home()
	{
		$this->load->view('smart_bazzar/index');
	}
	

	public function kategori_produk($kodeKategori,$slugKategori,$merek)
	{
		$this->db->select('master_product.*,master_category.id as idKategori,master_category.cat_slug,master_category.merek_slug')		
			 ->join('master_category','master_product.id_category=master_category.id')
			 ->where('master_category.cat_name','Jam Tangan')
			 ->where('master_category.status_temp',1);
        $dax= $this->db->get('master_product');
        $data['jumlahKategoriJam']=count($dax->result());

        $this->db->select('master_product.*,master_category.id as idKategori,master_category.cat_slug,master_category.merek_slug')		
			 ->join('master_category','master_product.id_category=master_category.id')
			 ->where('master_category.cat_name','Tas Wanita')
			 ->where('master_category.status_temp',1);
        $dax2= $this->db->get('master_product');
        $data['jumlahKategoriTasWanita']=count($dax2->result());

		$this->db->select('master_product.*,master_category.id as idKategori,master_category.cat_slug,master_category.merek_slug')		
			 ->join('master_category','master_product.id_category=master_category.id')
			 ->where('master_category.cat_name','Sepatu & Sandal')
			 ->where('master_category.status_temp',1);
        $dax3= $this->db->get('master_product');
        $data['jumlahKategoriSepatu']=count($dax3->result());








		$this->db->select('*');
		$this->db->where('cat_slug',"jam-tangan");
		$this->db->where('status_temp',1);
        $data['kategoriJam']= $this->db->get('master_category');


        $this->db->select('*');
		$this->db->where('cat_slug',"sepatu-dan-sandal");
		$this->db->where('status_temp',1);
        $data['kategoriSepatu']= $this->db->get('master_category');

        $this->db->select('*');
		$this->db->where('cat_slug',"tas-wanita");
		$this->db->where('status_temp',1);
        $data['kategoriTasWanita']= $this->db->get('master_category');

		$this->load->view('smart_bazzar/produk',$data);
	}

	public function sort_byPrice($kategori,$param){

		$jumlahParameter=substr_count($param,"_");
		$patternAwal="(.*)";
		$patternDinamis="_(.*)";
		for($i=0,$hitungJumahPattern=1,$patternGabung="";$i<$jumlahParameter;$i++)
		{
			$patternGabung.=$patternDinamis;
			$hitungJumahPattern++;
		}
		//echo  $hitungJumahPattern."<br/>";
		$patternFinal=$patternAwal."".$patternGabung;
		preg_match("/".$patternFinal."/",$param,$hasil);
		for($j=1,$tampungParam="";$j<=$hitungJumahPattern;$j++)
		{
			$tampungParam.="BETWEEN ".$hasil[$j]." OR eclast_price ";
		}
	 
		$replaceOne=str_replace("-"," AND ",$tampungParam);
		$whereCond=preg_replace("~ OR eclast_price (?!.* OR eclast_price )~","",$replaceOne);
 

		$sql = 'SELECT * FROM master_product WHERE id_category='.$kategori.' AND eclast_price '.$whereCond.' ORDER BY id_category ASC';

		/*echo $sql;
		die;*/
	 
    $query = $this->db->query($sql);


    

echo json_encode($query->result(),JSON_PRETTY_PRINT);
		
		
  
	}


	public function sort_byWarna($kategori,$param){

		$jumlahParameter=substr_count($param,"_");
		$patternAwal="(.*)";
		$patternDinamis="_(.*)";
		for($i=0,$hitungJumahPattern=1,$patternGabung="";$i<$jumlahParameter;$i++)
		{
			$patternGabung.=$patternDinamis;
			$hitungJumahPattern++;
		}
		//echo  $hitungJumahPattern."<br/>";
		$patternFinal=$patternAwal."".$patternGabung;
		preg_match("/".$patternFinal."/",$param,$hasil);
		for($j=1,$tampungParam="";$j<=$hitungJumahPattern;$j++)
		{
			$tampungParam.="LIKE '%".$hasil[$j]."%' OR name ";
		}

		//echo $tampungParam;
		//die; 
	 
		$replaceOne=str_replace("-"," AND ",$tampungParam);
		$whereCond=preg_replace("~ OR name (?!.* OR name )~","",$replaceOne);
		$sql = 'SELECT * FROM master_product WHERE id_category='.$kategori.' AND name '.$whereCond.' ORDER BY id_category ASC';
		//echo $sql;
	
    $query = $this->db->query($sql);
echo json_encode($query->result(),JSON_PRETTY_PRINT);
	}




	public function jsonRangeHargax($param){

		$jumlahParameter=substr_count($param,"_");
		$patternAwal="(.*)";
		$patternDinamis="_(.*)";
		for($i=0,$hitungJumahPattern=1,$patternGabung="";$i<$jumlahParameter;$i++)
		{
			$patternGabung.=$patternDinamis;
			$hitungJumahPattern++;
		}
		//echo  $hitungJumahPattern."<br/>";
		$patternFinal=$patternAwal."".$patternGabung;
		preg_match("/".$patternFinal."/",$param,$hasil);
		for($j=1,$tampungParam="";$j<=$hitungJumahPattern;$j++)
		{
			$tampungParam.="BETWEEN ".$hasil[$j]." OR eclast_price ";
		}
		//echo $tampungParam;
		$replaceOne=str_replace("-"," AND ",$tampungParam);
		$whereCond=preg_replace("~ OR eclast_price (?!.* OR eclast_price )~","",$replaceOne);
/*		$kueri="SELECT * FROM `master_product` WHERE eclast_price ".$whereCond;
		$query["mhs"]= $this->db->query("SELECT * FROM master_product");
		return $query->result();
*/

		

		$sql = 'SELECT * FROM master_product WHERE eclast_price '.$whereCond.' ORDER BY id_category ASC';


 
		/*echo $sql;
		die;*/
    $query = $this->db->query($sql);
    // Fetch the result array from the result object and return it
   /* print_r($query->result());*/

		//echo json_encode($this->db->query($kueri),JSON_PRETTY_PRINT);


		//$this->db->query("SELECT * FROM `mahasiswa` WHERE  mhsIdJurusan = $idJurusan");
		//echo $replaceOne;

echo json_encode($query->result(),JSON_PRETTY_PRINT);
		
		die;



		//SELECT * FROM `master_product` WHERE eclast_price between 100000 and 200000 or eclast_price between 300000 AND 500000


		//$jumlahParameter=1;
/*		$dataRooms.='{"roomName":"'.$room_name.'","bedType":"'.$bed_type.'","priceNet":'.$NetPrice.',"priceMarkup":'.$price_markup.',"bfType":"'.$bf_type.'","idRoom":'.$city_code.'},';
*/


		$dataParam='[{"paramX":"'.$patternFinal.'"}]';
		echo $dataParam;
	}
	public function telegram_sends()
	{
 $url = "https://wasap.ga/api/v1/?token=DbZ2wRnAm1l0pYNZ";
     $data = array(
       'service' => "email",
       'message' => "Halo Dunia!",
       'user' => "yasinmuhajirin@gmail.com"
     );
     $options = array(
       'http' => array(
         'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
         'method'  => 'POST',
         'content' => http_build_query($data),
       )
     );
     $context  = stream_context_create($options);
     $result = file_get_contents($url, false, $context);
 
     echo  "$result";

	}
	public function jsonRangeHarga($param)
	{
		//$key = $this->input->post('q');
		//echo "it works";

		$this->db->select('*');
	  //  $this->db->limit(12);
		$this->db->where('id_category','1');
		$this->db->where('eclast_price >=', 100000);
		$this->db->where('eclast_price <=', 200000);
		/*$this->db->where('eclast_price >=', 1000000);
		$this->db->where('eclast_price <=', 2000000);*/
		//$this->db->where('eclast_price','');
        $this->db->order_by('name', 'DESC');
        $query= $this->db->get('master_product');
        echo json_encode($query->result(),JSON_PRETTY_PRINT);
	}
	public function saveSignUp()
    {
        //echo $_POST["name"];
        $username=$_POST["email"];
        $password=$_POST["password"];


        $data = array(
            'username' => $username,
            'password' => $password
        );

        $this->db->insert('master_customer', $data);

        echo "it works";
    }
    public function signup()
    {
        $this->load->view('smart_bazzar/signup');
    }
	public function index()
	{
		$this->db->select('master_product.*,master_category.id as idKategori,master_category.cat_slug,master_category.merek_slug')		
			 ->join('master_category','master_product.id_category=master_category.id')
			 ->where('master_category.cat_name','Jam Tangan')
			 ->where('master_category.status_temp',1);
        $dax= $this->db->get('master_product');
        $data['jumlahKategoriJam']=count($dax->result());

        $this->db->select('master_product.*,master_category.id as idKategori,master_category.cat_slug,master_category.merek_slug')		
			 ->join('master_category','master_product.id_category=master_category.id')
			 ->where('master_category.cat_name','Tas Wanita')
			 ->where('master_category.status_temp',1);
        $dax2= $this->db->get('master_product');
        $data['jumlahKategoriTasWanita']=count($dax2->result());

		$this->db->select('master_product.*,master_category.id as idKategori,master_category.cat_slug,master_category.merek_slug')		
			 ->join('master_category','master_product.id_category=master_category.id')
			 ->where('master_category.cat_name','Sepatu & Sandal')
			 ->where('master_category.status_temp',1);
        $dax3= $this->db->get('master_product');
        $data['jumlahKategoriSepatu']=count($dax3->result());




		$this->db->select('*');
		$this->db->where('cat_name','Jam Tangan');
		$this->db->where('status_temp',1);
        $data['kategoriJam']= $this->db->get('master_category');

        $this->db->select('*');
		$this->db->where('cat_name','Sepatu & Sandal');
		$this->db->where('status_temp',1);
        $data['kategoriSepatu']= $this->db->get('master_category');


        $this->db->select('*');

		$this->db->where('cat_name','Tas Wanita');
		$this->db->where('status_temp',1);
        $data['kategoriTasWanita']= $this->db->get('master_category');
       
       
 
		$this->load->view('smart_bazzar/index',$data);

	}
	public function cekKabupaten()
	{

$provinsi_id = $_GET['prov_id'];

$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => "http://api.rajaongkir.com/starter/city?province=$provinsi_id",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "key: de9f095b6930016a15eba1cd42fee322"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  //echo $response;
}
$temp="";
$data = json_decode($response, true);
for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
	if($i==0)
	{
        $temp="yng pertama";
	}
	else
	{
		$temp=$data['rajaongkir']['results'][$i-1]['city_name'];
	}
	//echo "temp ".$temp." =";


	if($temp==$data['rajaongkir']['results'][$i]['city_name'])
	{
		continue;
	}
	else
	{
	  echo "<option value='".$data['rajaongkir']['results'][$i]['city_id']."' name='".$data['rajaongkir']['results'][$i]['city_name']."'>".$data['rajaongkir']['results'][$i]['city_name']."</option>";	
	}


/*
	$temp=$data['rajaongkir']['results'][$i-1]['city_name'];*/
    
}
	}
	public function ongkir($code)
	{
	
			$decode= base64_decode($code);

		$code_dec=preg_replace('/(.*?)#/', '',$decode);
		$code_dec2=preg_replace('/\/(.*?)/', '',$code_dec);


/*
		echo $code_dec2;
		die;*/
		//ECL-TSW-226535998

		$code_dec3=substr($code_dec2,0,17);
/*		echo "<br/>";

		echo "-".$code_dec2."-";
	
		*/
		/*
		$this->db->select('*');
		$this->db->where('eclast_code',$code_dec3);
        $data['dataProduk']=$this->db->get('master_product');
*/
        $this->db->select('master_product.*,master_category.id as idKategori,master_category.cat_slug,master_category.merek_slug')		
			 ->join('master_category','master_product.id_category=master_category.id')
			 ->where('master_product.eclast_code',$code_dec3);
        $data['dataProduk']= $this->db->get('master_product');





        /*$r= $this->db->get('master_product');

        foreach ($r->result() as $baris)
        {
        	echo $baris->eclast_code;
        }



/*        print_r($data['dataProduk']);
        die;*/

		//echo $_GET["code"];
		//die; 
		$this->load->view('smart_bazzar/ongkir',$data);
	}
	public function tampilkanSemuaProduk()
	{
		$this->db->select('*');
		$this->db->limit(12);
        $data['dataProduk']= $this->db->get('master_product');
        //echo json_encode($query->result(),JSON_PRETTY_PRINT);
        $this->load->view('smart_bazzar/products',$data);
	}
		public function jsonData($id_category)
	{


		$this->db->select('master_product.*,master_category.cat_slug,master_category.merek_slug')
		         ->from('master_product')
		         ->join('master_category','master_product.id_category=master_category.id')
		         ->where('master_product.id_category',$id_category);
		$result = $this->db->get();
		//$this->db->limit(12);
/*		$this->db->limit(8);
		$this->db->where('id_category',$id_category);
		*/


        //$query= $this->db->get('master_product');
        echo json_encode($result->result(),JSON_PRETTY_PRINT);
      
	}
	public function kategoriProduct($param){
		$this->db->select('*');

		//$this->db->limit(12);
		$this->db->where('id_category',$param);

        $query= $this->db->get('master_product');
        echo json_encode($query->result(),JSON_PRETTY_PRINT);
      
	}
	public function detail($id,$slug)

	{
/*		echo $id;
		echo "<br/>".$slug;*/
		$this->db->select('master_product.*,master_category.*')
		    // ->from('master_product')
			 ->join('master_category','master_product.id_category=master_category.id')
			 ->where('master_product.id',$id);
       //$data['dataProduk']= $this->db->get('master_product');

        $dat=$this->db->get('master_product');



        foreach($dat->result() as $row)
        {
        	//echo $row->cat_slug;
        	$this->db->select('master_product.*,master_category.*')
		    // ->from('master_product')
			 ->join('master_category','master_product.id_category=master_category.id')
			 ->where('master_category.merek_slug',$row->merek_slug)
			 ->limit(7);
			
			//$dat2=$this->db->get('master_product');

			$data['dataProdukSerupa']= $this->db->get('master_product');

/*
        foreach($dat2->result() as $row)
        {
        	echo $row->merek_slug."<br/>";

        }
*/
        }
        //die;
        $this->db->select('master_product.*,master_category.*')
		    // ->from('master_product')
			 ->join('master_category','master_product.id_category=master_category.id')
			 ->where('master_product.id',$id);
       $data['dataProduk']= $this->db->get('master_product');


        //$query=$this->db->get('master_product');
        //print_r($query->result());

		$this->load->view('smart_bazzar/single',$data);

	}
		public function jsonData2()
	
	{
		$this->db->select('*');
	  //  $this->db->limit(12);
		$this->db->where('id_category','2');
        $this->db->order_by('name', 'DESC');
        $query= $this->db->get('master_product');
        echo json_encode($query->result(),JSON_PRETTY_PRINT); 
	}
	public function order()
	{
/*		echo "it works";
		echo $_POST["nama"];
		echo $_POST["noHP"];
		echo $_POST["ambilProvinsi"];
		echo $_POST["ambilKabupaten"];
		echo $_POST["alamat"];
		echo $_POST["noOrder"];
		echo $_POST["harga"];
		echo $_POST["totalBayar"];*/

		
		$text='';
		$text='<b>No Order :'.$_POST["noOrder"].'</b><b>Nama : '.$_POST["nama"].'</b><b>Alamat '.$_POST["alamat"].'</b><b>Total Bayar : '.$_POST["totalBayar"].'</b></pre>';

		


		$url = 'https://api.telegram.org/bot462989956:AAEFIUgRVc5GRpzzxO1hG6F1nGyUR6iWaYs/sendMessage?chat_id=256823672&text='.$text.'&parse_mode=html';
$result = file_get_contents($url);
sleep(2);

die;

		$url2 = 'https://api.telegram.org/bot462989956:AAEFIUgRVc5GRpzzxO1hG6F1nGyUR6iWaYs/sendMessage?chat_id=256823672&text=https://www.eclast-store.com/storage/sepatu/ecl-sp-246849181.jpg';
$result2 = file_get_contents($url2);

sleep(2);

$url3 = 'https://api.telegram.org/bot462989956:AAEFIUgRVc5GRpzzxO1hG6F1nGyUR6iWaYs/sendMessage?chat_id=256823672&text=Terima kasih Sista Angeline atas pemesanan tas shopie martin black dengan No order : ECLORD-2141-189201 untuk pembayaran bisa di transfer di rekening BCA atas nama eclast-store dengan nominal Rp 356.900.setelah bayar kamu bisa konfirmasi lewat SMS/WA ke nomer ini dengan memberikan info no order dan nominal total transfer. Sekian Maacih sista';
$result3 = file_get_contents($url3);


/*echo "Terima kasih Sista Angeline atas pemesanan tas shopie martin black dengan No order : ECLORD-2141-189201 untuk pembayaran bisa di transfer di rekening BCA atas nama eclast-store dengan nominal Rp 356.900,00. setelah bayar kamu bisa konfirmasi lewat SMS/WA ke nomer ini dengan memberikan info no order dan nominal total transfer. Sekian yah. Happy Shopping And Have a nice day";*/

/*$result = json_decode($result, true);

var_dump($result['result']);*/


	}
	public function getOrderForm($kodePesan,$nama,$asal,$provinsi,$kabupaten,$noHP,$harga,$alamat,$kodePesan,$ongkir)
	{
/*		echo $kodePesan;
		echo $nama;
		echo $asal;
		echo $provinsi;
		echo $kabupaten;
		echo $noHP;
		echo $harga;
		echo $alamat;
		echo $kodePesan;
		echo $ongkir;
		"Terima kasih agan sista ".$nama." atas ordernya dengan no ".kodePesan." dengan total harga + ongkir Rp ".$harga+$ongkir." dapat anda transfer ke rekening kami dibawah ini. BRI : 6013 0133 5002 6543 Atas Nama : Denta Zamzam Yasin Muhajir Setelah anda mengirim bisa konfirmasi ke nomor ini";
*/
		$infoice="Terima kasih agan sista ".$nama." atas ordernya dengan no ".$kodePesan." dengan total harga + ongkir Rp ".$harga." dapat anda transfer ke rekening kami dibawah ini. BRI : 6013 0133 5002 6543 Atas Nama : Denta Zamzam Yasin Muhajir Setelah anda mengirim bisa konfirmasi ke nomor ini ";

	
		$url = 'https://api.telegram.org/bot462989956:AAEFIUgRVc5GRpzzxO1hG6F1nGyUR6iWaYs/sendMessage?chat_id=256823672&text='.$infoice.'&parse_mode=html';
		$result = file_get_contents($url);
        sleep(1);

		$url2 = 'https://api.telegram.org/bot462989956:AAEFIUgRVc5GRpzzxO1hG6F1nGyUR6iWaYs/sendMessage?chat_id=256823672&text='.$noHP.'&parse_mode=html';
		$result2 = file_get_contents($url2);

		$dataParam='[{"kodePesan":"'.$kodePesan.'"}]';
		echo $dataParam;
		die;



		$text='- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - No Order :<b>'.$kodePesan.'</b>';
		$text2='Nama :<b>'.$nama.'</b>';
		$text3='Alamat :<b>'.$alamat.'-'.$provinsi.'-'.$kabupaten.'</b>';

	
		$url = 'https://api.telegram.org/bot462989956:AAEFIUgRVc5GRpzzxO1hG6F1nGyUR6iWaYs/sendMessage?chat_id=256823672&text='.$text.'&parse_mode=html';
		$result = file_get_contents($url);
		//sleep(1);

		$url2 = 'https://api.telegram.org/bot462989956:AAEFIUgRVc5GRpzzxO1hG6F1nGyUR6iWaYs/sendMessage?chat_id=256823672&text='.$text2.'&parse_mode=html';
		$result = file_get_contents($url2);
		//sleep(1);
				$url3 = 'https://api.telegram.org/bot462989956:AAEFIUgRVc5GRpzzxO1hG6F1nGyUR6iWaYs/sendMessage?chat_id=256823672&text='.$text3.'&parse_mode=html';
		$result = file_get_contents($url3);


		$dataParam='[{"kodePesan":"'.$kodePesan.'"}]';
		echo $dataParam;
	}
	public function test()
	{
		$this->load->view('smart_bazzar/testAjax');	
	}
	public function smsTransfer()
	{

$userkey = "slgkwv";
$passkey = "zcbsdbsdgwgwet23";
$nohp = "089673309612";
$message = "ID Pesanan : 655321, Dengan Total 782.021 Transfer ke rekening BCA atas nama xxxxx.. ";
$url = "https://reguler.zenziva.net/apps/smsapi.php";
$curlHandle = curl_init();
curl_setopt($curlHandle, CURLOPT_URL, $url);
curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$nohp.'&pesan='.urlencode($message));
curl_setopt($curlHandle, CURLOPT_HEADER, 0);
curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
curl_setopt($curlHandle, CURLOPT_POST, 1);
$results = curl_exec($curlHandle);
header('Content-Type: text/xml');
print($results);
curl_close($curlHandle);


	}
	public function cek_ongkir()
	{
		$asal = $_POST['asal'];
	$id_kabupaten = $_POST['kab_id'];
	$kurir = $_POST['kurir'];
	$berat = $_POST['berat'];

	$curl = curl_init();
	curl_setopt_array($curl, array(
	  CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => "origin=".$asal."&destination=".$id_kabupaten."&weight=".$berat."&courier=".$kurir."",
	  CURLOPT_HTTPHEADER => array(
	    "content-type: application/x-www-form-urlencoded",
	    "key: de9f095b6930016a15eba1cd42fee322"
	  ),
	));

	$response = curl_exec($curl);



	$err = curl_error($curl);

	curl_close($curl);



$data = json_decode($response, true);

/*echo count($data['rajaongkir']['results']);*/
//echo $data['rajaongkir']['results'][0]['code'];
$gabung = "";

for ($i=0; $i < count($data['rajaongkir']['results']); $i++) { 
    //echo $data['rajaongkir']['results'][$i]['name'];
    //echo count($data['rajaongkir']['results'][$i]['costs']);

    for($j=0;$j<count($data['rajaongkir']['results'][$i]['costs']);$j++)
    {
    	/*if($data['rajaongkir']['results'][0]['costs'][$j]['service']="OKE")
    	{
    		continue;
    	}*/
    	//echo $data['rajaongkir']['results'][0]['costs'][$j]['service']," = ";
    	$gabung.= $data['rajaongkir']['results'][0]['costs'][$j]['service']." = ";
    	for($k=0;$k<count($data['rajaongkir']['results'][0]['costs'][$j]['cost']);$k++)
    	{
    		//echo $data['rajaongkir']['results'][0]['costs'][$j]['cost'][$k]['value'];
    		$gabung.=$data['rajaongkir']['results'][0]['costs'][$j]['cost'][$k]['value'].",";
    	}
    }
    preg_match('/REG\s=\s(.*?),/',$gabung,$result);
    echo $result[1];
}
/*
for ($i=0; $i < count($data['rajaongkir']['results']); $i++) { 
    echo "<option value='".$data['rajaongkir']['results'][$i]['city_id']."'>".$data['rajaongkir']['results'][$i]['city_name']."</option>";
}



	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  echo $response;
	}**/
	}


}