<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="en">
<head>
<title>Eclast Store | New Look & Always Bold</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Smart Bazaar Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="/asset/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="/asset/css/style.css" rel="stylesheet" type="text/css" media="all" /> 
<link href="/asset/css/animate.min.css" rel="stylesheet" type="text/css" media="all" /><!-- animation -->
<link href="/asset/css/menu.css" rel="stylesheet" type="text/css" media="all" /> <!-- menu style -->
<link href='//fonts.googleapis.com/css?family=Risque' rel='stylesheet' type='text/css'>

<!-- //Custom Theme files -->
<!-- font-awesome icons -->
<link href="/asset/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="/asset/js/jquery-2.2.3.min.js"></script> 
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@1.6.0/src/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@1.6.0/extras/loadingoverlay_progress/loadingoverlay_progress.min.js"></script>
<script src="/asset/js/jquery-scrolltofixed-min.js" type="text/javascript"></script><!-- fixed nav js -->


<script type="text/javascript">

	$(document).ready(function(){
	

		$('#provinsi').change(function(){

			//Mengambil value dari option select provinsi kemudian parameternya dikirim menggunakan ajax 
			var prov = $('#provinsi').val();
			$("#ambilProvinsi").val(prov);

      		$.ajax({
            	type : 'GET',
           		url : '<?php echo site_url('Product/cekKabupaten')?>',
            	data :  'prov_id=' + prov,
					success: function (data) {

					//jika data berhasil didapatkan, tampilkan ke dalam option select kabupaten
					$("#kabupaten").html(data);
					
				}
          	});
		});
		$("#kabupaten").change(function(){
			//alert("it works");
			//Mengambil value dari option select provinsi asal, kabupaten, kurir, berat kemudian parameternya dikirim menggunakan ajax 
			//var asal = $('#asal').val();
			var asal =22;
			var kab = $('#kabupaten').val();
			$("#ambilKabupaten").val(kab);
			//var kurir = $('#kurir').val();
			//var berat = $('#berat').val();
			var kurir = "jne";
			var berat = 1000;

      		$.ajax({
            	type : 'POST',
				url : '<?php echo site_url('Product/cek_ongkir')?>',
            	data :  {'kab_id' : kab, 'kurir' : kurir, 'asal' : asal, 'berat' : berat},
					success: function (data) {

						//var id=response[i].id;

						



					//jika data berhasil didapatkan, tampilkan ke dalam element div ongkir
					var data2=data;
					$("#ongkirValue").val(data2);

						var	number_string2 = data2.toString(),sisa2 	= number_string2.length % 3,rupiah2 	= number_string2.substr(0, sisa2),ribuan2 	= number_string2.substr(sisa2).match(/\d{3}/g);
		
if (ribuan2) {
	separator2 = sisa2 ? '.' : '';
	rupiah2 += separator2 + ribuan2.join('.');
}
					$("#ongkir").text(rupiah2);
					
					var harga=$("#hargaxx").text();
					var hasil=0;
					hasil=parseFloat(data)+parseFloat(harga);
					var	number_string = hasil.toString(),sisa 	= number_string.length % 3,rupiah 	= number_string.substr(0, sisa),ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
		
if (ribuan) {
	separator = sisa ? '.' : '';
	rupiah += separator + ribuan.join('.');
}
				


					$("#totalPem").text(rupiah);
					$("#totalBayar").val(rupiah);
					
					
				}
          	});
		});

		$("#cek").click(function(){
			//Mengambil value dari option select provinsi asal, kabupaten, kurir, berat kemudian parameternya dikirim menggunakan ajax 
			//var asal = $('#asal').val();

			var asal=22;
			var kab = $('#kabupaten').val();
			var kurir = $('#kurir').val();
			var berat = $('#berat').val();

      		$.ajax({
            	type : 'POST',
           		url : 'http://testing.lokal/cek_ongkir.php',
            	data :  {'kab_id' : kab, 'kurir' : kurir, 'asal' : asal, 'berat' : berat},
					success: function (data) {

					//jika data berhasil didapatkan, tampilkan ke dalam element div ongkir
					$("#ongkir").text(data);
				}
          	});
		});
	});
</script>
<script>
    $(document).ready(function() {

        // Dock the header to the top of the window when scrolled past the banner. This is the default behaviour.

        $('.header-two').scrollToFixed();  
        // previous summary up the page.

        var summaries = $('.summary');
        summaries.each(function(i) {
            var summary = $(summaries[i]);
            var next = summaries[i + 1];

            summary.scrollToFixed({
                marginTop: $('.header-two').outerHeight(true) + 10, 
                zIndex: 999
            });
        });
    });
</script>
<!-- //js --> 
<!-- web-fonts -->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lovers+Quarrel' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Offside' rel='stylesheet' type='text/css'> 
<!-- web-fonts -->  
<!-- start-smooth-scrolling -->
<script type="text/javascript" src="/asset/js/move-top.js"></script>
<script type="text/javascript" src="/asset/js/easing.js"></script>	

<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!-- //end-smooth-scrolling -->
<!-- smooth-scrolling-of-move-up -->
<script type="text/javascript">
	$(document).ready(function() {
	
		var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
		};
		
		$().UItoTop({ easingType: 'easeOutQuart' });
		
	});
</script>
<!-- //smooth-scrolling-of-move-up -->
</head>
<body>
	<!-- header -->   

  <!-- header --><div class="header" >
    <div class="w3ls-header" style="background-color:black;"><!--header-one--> 
      <div class="w3ls-header-left">
        <p><a href="#">Jam kerja : Senin - Jumat | 08.00 - 21.00 </a></p>
      </div>
      <div class="w3ls-header-right">
        <ul>

         <li class="dropdown head-dpdn">
            <a href="help.html" class="dropdown-toggle"><i class="fa fa-facebook" aria-hidden="true"></i> eclaststore</a>
          </li> 
         <li class="dropdown head-dpdn">
            <a href="help.html" class="dropdown-toggle"><i class="fa fa-whatsapp" aria-hidden="true"></i> 081-385-800-400</a>
          </li> 

          <li class="dropdown head-dpdn">
            <a href="card.html" class="dropdown-toggle"><i class="fa fa-instagram" aria-hidden="true"></i> eclaststore</a>
          </li>
          <li class="dropdown head-dpdn">
            <a href="contact.html" class="dropdown-toggle"><i class="fa fa-envelope" aria-hidden="true"></i> bantuan@eclast-store.com</a>
          </li>  
         
        </ul>
      </div>


      <div class="clearfix"> </div> 
    </div>

    <div class="header-two"><!-- header-two -->
      <div class="container">
         <div class="header-logo">
          <h1><a href="/"><span style="">E</span>clast<i><sub>_store.com</sub></i></a></h1>
          <h6>New Look & Always Bold.</h6> 
        </div> 
        <div class="header-search" style="">
          <form action="#" method="post">
            <input type="search" name="Search" placeholder="Cari aja disini..." required="">
            <button type="submit" class="btn btn-default" aria-label="Left Align">
              <i class="fa fa-search" aria-hidden="true"> </i>
            </button>
          </form>
        </div>
        <div class="header-cart" style="">
          <div class="row">
         <div class="col-md-12">
           <div class="row">

          <div class="col-md-12">
            <center><h3> Customer Services</h3></center>
          </div>
          <div class="col-md-12" style="height:5px">
  
          </div>

          <div class="col-md-6">
           <center><h4 style="color:white;background-color:green"><i class="fa fa-whatsapp" aria-hidden="true" style="color:white;background-color:green"></i> 081 385 800 400</h4></center>
          </div>
          <div class="col-md-6">
            <center><h4 style="color:black;background-color:yellow"><i class="fa fa-wechat" aria-hidden="true" style="color:black;background-color:yellow"></i> @eclaststore</h4></center>
          </div>


        </div>



         </div>
       </div>
          <div class="clearfix" > </div> 
        </div> 

        <div class="clearfix"> </div>
      </div>    
    </div><!-- //header-two -->
    

                  <div id="top-merch"></div> 
<?php
include('include/menu.php');
?>
	<!-- //header --> 	
	<!-- contact-page -->
	<div class="contact">
		<div class="container"> 
			 <div class="contact-form-row">
				<h3 class="w3ls-title w3ls-title1">Form Order Barang</h3>  
				<div class="col-md-7 contact-left">
					<!-- <form action=" Product/order " method="post"> -->
	<input type="text" name="nama" id="nama" placeholder="Nama Lengkap" required="">
						<input class="email" id="noHP" type="text" name="noHP" placeholder="Nomor Whatsapp / HP " required="">
	

<?php

	//Get Data Kabupaten
	/*
	$curl = curl_init();	
	curl_setopt_array($curl, array(
	  CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "key: de9f095b6930016a15eba1cd42fee322"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	
	echo "<select class='form-control' name='asal' id='asal' required=''>";
	echo "<option >Pilih Kota Asal</option>";
		$data = json_decode($response, true);
		for ($i=0; $i < count($data['rajaongkir']['results']); $i++) { 
		    echo "<option value='".$data['rajaongkir']['results'][$i]['city_id']."' name='".$data['rajaongkir']['results'][$i]['city_name']."'>".$data['rajaongkir']['results'][$i]['city_name']."</option>";
		}
	echo "</select><br>";*/
	//Get Data Kabupaten


	//-----------------------------------------------------------------------------

	//Get Data Provinsi
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "key: de9f095b6930016a15eba1cd42fee322"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	
	echo "<select class='form-control' name='provinsi' id='provinsi' required=''>";
	echo "<option value='' style='color:silver'>Pilih Provinsi Tujuan</option>";
	$data = json_decode($response, true);
	for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
		echo "<option value='".$data['rajaongkir']['results'][$i]['province_id']."' name='".$data['rajaongkir']['results'][$i]['province']."'>".$data['rajaongkir']['results'][$i]['province']."</option>";
	}
	echo "</select><br>";
	//Get Data Provinsi

?>
					 

						
		<select class="form-control" id="kabupaten" name="kabupaten" required="">
			<option value="">Pilih Kab/kota Tujuan</option>
		</select><br>
						<textarea placeholder="Alamat Lengkap" id="alamat" name="alamat" required=""></textarea>
						<br/>
						<br/>
						<input type="text" placeholder="Keterangan (optional)" required="">
						
			
				</div> 

				<div class="col-md-4 contact-right" >
					 
				
					 
<div class="row">

	<div class="col-md-12 address-left agileinfo" style="background-color:RGBA(220,220,220,0.5)">
					<div class="footer-logo header-logo" style="background-color:white;">

<?php foreach($dataProduk->result() as $baris)
{

$folder=strtoupper($baris->cat_slug."-".$baris->merek_slug);


	?>
	<div class="row">
<div class="col-md-12" style="">
	
<div class="col-md-4" style="">
	
	<img src="/storage/<?=$folder?>/<?=$baris->main_image;?>.jpg" class="img-responsive" height="100%" width="100%" alt="img"/>
	
</div>
<div class="col-md-8"><br/><h6><?=$baris->name;?>

</h6>

	</div>
</div>
</div>
<?php
}
?>
		
					</div>
					<ul>

<?php
$eclastCode='ECL-'.date("i").''.rand(1,9).'-'.date("s").''.rand(1,9);
?>		
<input type="hidden" id="kodePesan" name="noOrder" value="<?=$eclastCode?>"/>

						<li><i style="float:left;">No Order</i><b> <?=$eclastCode;?></b></li>

						<li><i style="float:left;">Harga</i>  <b>Rp</b> 

							<b id="hargax"><?php
foreach($dataProduk->result() as $baris)
{
	?>
	<div id="hargaxx" style="display:none"><?=$baris->eclast_price;?>
		<input type="hidden" id="harga_eclast" name="harga" value="<?=$baris->eclast_price;?>"/>
<input type="hidden" id="totalBayar" name="totalBayar" />
<input type="hidden" id="ambilKabupaten" name="ambilKabupaten" />
<input type="hidden" id="ambilProvinsi" name="ambilProvinsi" />
	</div>
	
	<?php
	echo number_format($baris->eclast_price,0,',','.');
}
?> </b></li>
						<li><i style="float:left;">Ongkir *</i><b>Rp</b> <b id="ongkir"> 0</b> </li>
						<input type="hidden" id="ongkirValue" value=""/>
						<!-- <li id="ongkir"><i style="float:left;">Ongkir</i>  Rp 28.000 </li> -->
						<li><i style="float:left;"><b style="color:red">Total</b></i> <b style="color:red">Rp </b><b style="color:red" id="totalPem">  <?php foreach($dataProduk->result() as $baris)
{
	echo number_format($baris->eclast_price,0,',','.');
}?></b></li>
					</ul>
					<center><input type="submit" class="btn btn-full" id="process" value="Konfirmasi Order"></center>
					
					<br/>
					<h6>* Kami menggunakan pengiriman jasa JNE Regular. &nbsp;&nbsp;&nbsp;Estimasi waktu pengiriman barang 3 - 6 hari</h6>
				</div>
</div>
<script>
$('#process').click(function(){
	      		$.LoadingOverlaySetup({
    color           : "rgba(0, 0, 0, 0.8)",
    image           : "http://datainflow.com/wp-content/uploads/2017/09/loader.gif",
    maxSize         : "80px",
    minSize         : "20px",
    resizeInterval  : 0,
    size            : "50%"
});
	/*alert("it works");
	return*/
	var nama=$('#nama').val();
	//var asal=$('#asal').find('option:selected').attr("name");
	var asal=22;
	var provinsi=$('#provinsi').find('option:selected').attr("name");
	var kabupaten=$('#kabupaten').find('option:selected').attr("name");
	var noHP=$('#noHP').val();
	var harga=$('#harga_eclast').val();
	var alamat=$('#alamat').val();
	var kodePesan=$('#kodePesan').val();
	var ongkir=$('#ongkirValue').val();
	/*alert(provinsi);
	return;*/
	

	     $.ajax({

        url: "<?php echo site_url('Product/getOrderForm/')?>"+kodePesan+"/"+nama+"/"+asal+"/"+provinsi+"/"+kabupaten+"/"+noHP+"/"+harga+"/"+alamat+"/"+kodePesan+"/"+ongkir,
        type: "get",
        dataType: "JSON",
         beforeSend: function() {
   
		$.LoadingOverlay("show");

              
           },
        success: function(response){
        	 $.LoadingOverlay("hide");
        	alert("sukses");


		}
});

});

	</script>


					 
	





					<div class="cnt-w3agile-row">


						
					</div>
				</div> 
				<div class="clearfix"> </div>	
			</div>
		</div>
	</div>
	<!-- //contact-page --> 
	<!-- footer-top -->
	<div class="w3agile-ftr-top">
		<div class="container">
			<div class="ftr-toprow">
				<div class="col-md-4 ftr-top-grids">
					<div class="ftr-top-left">
						<i class="fa fa-truck" aria-hidden="true"></i>
					</div> 
					<div class="ftr-top-right">
						<h4>FREE DELIVERY</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
					</div> 
					<div class="clearfix"> </div>
				</div> 
				<div class="col-md-4 ftr-top-grids">
					<div class="ftr-top-left">
						<i class="fa fa-user" aria-hidden="true"></i>
					</div> 
					<div class="ftr-top-right">
						<h4>CUSTOMER CARE</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
					</div> 
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-4 ftr-top-grids">
					<div class="ftr-top-left">
						<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
					</div> 
					<div class="ftr-top-right">
						<h4>GOOD QUALITY</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
					</div>
					<div class="clearfix"> </div>
				</div> 
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //footer-top --> 
 
	<!-- footer -->
	 
  <!-- //footer -->
  <div class="copy-right" style="background-color:black;"> 
    
      <p>© 2018 Minor Mood Works&trade; | All rights reserved</p>
   
  </div> 
	<!-- cart-js -->
	<script src="/asset/js/minicart.js"></script>

	<script>

        w3ls.render();

        //alert("dsds"+subtotal());

        w3ls.cart.on('w3sb_checkout', function (evt) {
        	var items, len, i;

        	if (this.subtotal() > 0) {
        		items = this.items();



        		for (i = 0, len = items.length; i < len; i++) {
        			items[i].set('shipping', 0);
        			items[i].set('shipping2', 0);
        		}
        	}

        });
    </script>  
	<!-- //cart-js -->
	<!-- menu js aim -->
	<script src="/asset/js/jquery.menu-aim.js"> </script>
	<script src="/asset/js/main.js"></script> <!-- Resource jQuery -->
	<!-- //menu js aim --> 
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/asset/js/bootstrap.js"></script>
</body>
</html>