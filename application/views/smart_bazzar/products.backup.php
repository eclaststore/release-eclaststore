
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->


<!DOCTYPE html>
<html lang="en">
<head>
<title>Smart Bazaar an E-commerce Online Shopping Category Flat Bootstrap Responsive Website Template | Products :: w3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Smart Bazaar Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="/asset/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="/asset/css/style.css" rel="stylesheet" type="text/css" media="all" /> 
<link href="/asset/css/menu.css" rel="stylesheet" type="text/css" media="all" />
<link href="/asset/css/loadingbar.css" rel="stylesheet" type="text/css" media="all" />
<link href="/asset/css/nprogress.css" rel="stylesheet" type="text/css" media="all" />

 <!-- menu style -->  
<link href="/asset/css/animate.min.css" rel="stylesheet" type="text/css" media="all" />   
<link href="/asset/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all"> <!-- carousel slider -->  

<!-- //Custom Theme files -->
<!-- font-awesome icons -->
<link href="/asset/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="/asset/js/jquery-2.2.3.min.js"></script> 
<script src="/asset/js/jquery.loadingbar.js"></script> 
<script src="/asset/js/nprogress.js"></script> 

<script src="/asset/js/owl.carousel.js"></script>
<!-- //js --> 
<!-- web-fonts -->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lovers+Quarrel' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Offside' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Tangerine:400,700' rel='stylesheet' type='text/css'>
<!-- web-fonts --> 
<!-- scroll to fixed--> 
<script src="/asset/js/jquery-scrolltofixed-min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {

        // Dock the header to the top of the window when scrolled past the banner. This is the default behaviour.

        $('.header-two').scrollToFixed();  
        // previous summary up the page.

        var summaries = $('.summary');
        summaries.each(function(i) {
            var summary = $(summaries[i]);
            var next = summaries[i + 1];

            summary.scrollToFixed({
                marginTop: $('.header-two').outerHeight(true) + 10, 
                zIndex: 999
            });
        });
    });
</script>
<!-- //scroll to fixed--> 
<!-- start-smooth-scrolling -->
<script type="text/javascript" src="/asset/js/move-top.js"></script>
<script type="text/javascript" src="/asset/js/easing.js"></script>	
<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!-- //end-smooth-scrolling -->
<!-- smooth-scrolling-of-move-up -->
<script type="text/javascript">
	$(document).ready(function() {
	
		var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
		};
		
		$().UItoTop({ easingType: 'easeOutQuart' });
		
	});
</script>
<!-- //smooth-scrolling-of-move-up -->  
<!-- the jScrollPane script -->
<script type="text/javascript" src="/asset/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" id="sourcecode">
	$(function()
	{
		$('.scroll-pane').jScrollPane();
	});
</script>
<!-- //the jScrollPane script -->
<script type="text/javascript" src="/asset/js/jquery.mousewheel.js"></script>
<!-- the mousewheel plugin --> 
</head><body>


	<!-- header -->
	<div class="header">
		<div class="w3ls-header"><!--header-one--> 
			<div class="w3ls-header-left">
				<p><a href="#">UPTO $50 OFF ON LAPTOPS | USE COUPON CODE LAPPY </a></p>
			</div>
			<div class="w3ls-header-right">
				<ul>
					<li class="dropdown head-dpdn">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true"></i> My Account<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="{{url('/redirect')}}">Login </a></li> 
							<li><a href="signup.html">Sign Up</a></li> 
							<li><a href="login.html">My Orders</a></li>  
							<li><a href="login.html">Wallet</a></li> 
						</ul> 
					</li> 
					<li class="dropdown head-dpdn">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-percent" aria-hidden="true"></i> Today's Deals<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="offers.html">Cash Back Offers</a></li> 
							<li><a href="offers.html">Product Discounts</a></li>
							<li><a href="offers.html">Special Offers</a></li> 
						</ul> 
					</li> 
					<li class="dropdown head-dpdn">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gift" aria-hidden="true"></i> Gift Cards<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="offers.html">Product Gift card</a></li> 
							<li><a href="offers.html">Occasions Register</a></li>
							<li><a href="offers.html">View Balance</a></li> 
						</ul> 
					</li> 
					<li class="dropdown head-dpdn">
						<a href="contact.html" class="dropdown-toggle"><i class="fa fa-map-marker" aria-hidden="true"></i> Store Finder</a>
					</li> 
					<li class="dropdown head-dpdn">
						<a href="card.html" class="dropdown-toggle"><i class="fa fa-credit-card-alt" aria-hidden="true"></i> Credit Card</a>
					</li> 
					<li class="dropdown head-dpdn">
						<a href="help.html" class="dropdown-toggle"><i class="fa fa-question-circle" aria-hidden="true"></i> Help</a>
					</li>
				</ul>
			</div>
			<div class="clearfix"> </div> 
		</div>
		<div class="header-two"><!-- header-two -->
			<div class="container">
				<div class="header-logo">
					<h1><a href="/"><span>S</span>mart <i>Bazaar</i></a></h1>
					<h6>Your stores. Your place.</h6> 
				</div>	
				<div class="header-search" style="border:1px dashed black;width">
					<form action="#" method="post">
						<input type="search" name="Search" placeholder="Search for a Product..." required="">
						<button type="submit" class="btn btn-default" aria-label="Left Align">
							<i class="fa fa-search" aria-hidden="true"> </i>
						</button>
					</form>
				</div>
				<div class="header-cart" style="border:1px dashed black;width">
          <div class="my-account">
            <div class="w3ls-header-right" >
        <ul>
          <li class="dropdown head-dpdn" >
            <a style="color:silver;" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true"></i> My Account<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="{{url('/redirect')}}">Login </a></li> 
              <li><a href="signup.html">Sign Up</a></li> 
  
            </ul> 
          </li> 
          <li class="dropdown head-dpdn">
            <a style="color:silver;" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-info" aria-hidden="true"></i> Info<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="offers.html">Cara Belanja</a></li> 
              <li><a href="offers.html">Ketentuan Retur</a></li>
              <li><a href="offers.html">Tentang Kami</a></li> 
            </ul> 
          </li> 
        </ul>
      </div>
           

        </div> 
       
					<div class="my-account">
						<div id="statusk">
							
						 
						</div>						
					</div>
				
					<div class="clearfix"> </div> 
				</div> 
				<div class="clearfix"> </div>
			</div>		
		</div><!-- //header-two -->
		



<?php
include('include/menu.php');
?>


	<!-- //header --> 	
	<!-- products -->

<!-- welcome -->
  <div class="welcome"> 
    <div class="container"> 
      <div class="welcome-info">
        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class=" nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" >
              <i class="fa fa-laptop" aria-hidden="true"></i> 
              <h5>Electronics</h5>
            </a></li>
            <li role="presentation"><a href="#carl" role="tab" id="carl-tab" data-toggle="tab"> 
              <i class="fa fa-female" aria-hidden="true"></i>
              <h5>Fashion</h5>
            </a></li>
            <li role="presentation"><a href="#james" role="tab" id="james-tab" data-toggle="tab"> 
              <i class="fa fa-gift" aria-hidden="true"></i>
              <h5>Photo & Gifts</h5>
            </a></li>
            <li role="presentation"><a href="#decor" role="tab" id="decor-tab" data-toggle="tab"> 
              <i class="fa fa-home" aria-hidden="true"></i>
              <h5>Home Decor</h5>
            </a></li>
            <li role="presentation"><a href="#sports" role="tab" id="sports-tab" data-toggle="tab"> 
              <i class="fa fa-motorcycle" aria-hidden="true"></i>
              <h5>Sports</h5>
            </a></li> 
          </ul>
          <div class="clearfix"> </div>
          <h3 class="w3ls-title">Produk Terlaris</h3>
          <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
              <div class="tabcontent-grids">  
                <div id="owl-demo" class="owl-carousel"> 
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products.html"><img src="images/e1.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products.html">Audio speaker</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$100</h5> 
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Audio speaker" /> 
                          <input type="hidden" name="amount" value="100.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>  
                      </div>   
                    </div>   
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits">
                      <div class="new-tag"><h6>Sale</h6></div>
                      <a href="products.html"><img src="images/e2.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products.html">Refrigerator</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p> 
                        <h5>$300</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Refrigerator" /> 
                          <input type="hidden" name="amount" value="300.00"/> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>       
                    </div>  
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>New</h6></div>
                      <a href="products.html"><img src="images/e3.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products.html">Smart Phone</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$70</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Smart Phone" /> 
                          <input type="hidden" name="amount" value="70.00"/> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div>  
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products.html"><img src="images/e4.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products.html">Digital Camera</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$80</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Digital Camera"/> 
                          <input type="hidden" name="amount" value="80.00"/>  
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>         
                    </div>  
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products.html"><img src="images/e1.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products.html">Audio speaker</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$100</h5> 
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Audio speaker" /> 
                          <input type="hidden" name="amount" value="100.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>  
                      </div>   
                    </div>   
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits">
                      <div class="new-tag"><h6>Sale</h6></div>
                      <a href="products.html"><img src="images/e2.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products.html">Refrigerator</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p> 
                        <h5>$300</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Refrigerator" /> 
                          <input type="hidden" name="amount" value="300.00"/>  
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>       
                    </div>  
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>New</h6></div>
                      <a href="products.html"><img src="images/e3.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products.html">Smart Phone</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$70</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Smart Phone" /> 
                          <input type="hidden" name="amount" value="70.00"/>
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div>  
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products.html"><img src="images/e4.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products.html">Digital Camera</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$80</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Digital Camera"/> 
                          <input type="hidden" name="amount" value="80.00"/> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>         
                    </div>  
                  </div>
                </div> 
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="carl" aria-labelledby="carl-tab">
              <div class="tabcontent-grids">
                <script>
                  $(document).ready(function() { 
                    $("#owl-demo1").owlCarousel({
                   
                      autoPlay: 3000, //Set AutoPlay to 3 seconds
                   
                      items :4,
                      itemsDesktop : [640,5],
                      itemsDesktopSmall : [414,4],
                      navigation : true
                   
                    });
                    
                  }); 
                </script>
                <div id="owl-demo1" class="owl-carousel"> 
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products1.html"><img src="images/f1.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products1.html">T Shirt</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$10</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="T Shirt" /> 
                          <input type="hidden" name="amount" value="10.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div>    
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits">
                      <div class="new-tag"><h6>20% <br> Off</h6></div>
                      <a href="products1.html"><img src="images/f2.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products1.html">Women Sandal</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$20</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Women Sandal" /> 
                          <input type="hidden" name="amount" value="20.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div> 
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products1.html"><img src="images/f3.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products1.html">Jewellery</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$60</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Jewellery" /> 
                          <input type="hidden" name="amount" value="60.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>         
                    </div> 
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>New</h6></div>
                      <a href="products1.html"><img src="images/f4.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products1.html">Party dress</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$15</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Party dress" /> 
                          <input type="hidden" name="amount" value="15.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>      
                    </div> 
                  </div> 
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products1.html"><img src="images/f1.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products1.html">T Shirt</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$10</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="T Shirt" /> 
                          <input type="hidden" name="amount" value="10.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div>    
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits">
                      <div class="new-tag"><h6>20% <br> Off</h6></div>
                      <a href="products1.html"><img src="images/f2.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products1.html">Women Sandal</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$20</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Women Sandal" /> 
                          <input type="hidden" name="amount" value="20.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div> 
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products1.html"><img src="images/f3.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products1.html">Jewellery</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$60</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Jewellery" /> 
                          <input type="hidden" name="amount" value="60.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>         
                    </div> 
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>New</h6></div>
                      <a href="products1.html"><img src="images/f4.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products1.html">Party dress</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$15</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Party dress" /> 
                          <input type="hidden" name="amount" value="15.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>      
                    </div> 
                  </div>   
                </div>   
              </div>
            </div> 
            <div role="tabpanel" class="tab-pane fade" id="james" aria-labelledby="james-tab">
              <div class="tabcontent-grids">
                <script>
                  $(document).ready(function() { 
                    $("#owl-demo2").owlCarousel({
                   
                      autoPlay: 3000, //Set AutoPlay to 3 seconds
                   
                      items :4,
                      itemsDesktop : [640,5],
                      itemsDesktopSmall : [414,4],
                      navigation : true
                   
                    });
                    
                  }); 
                </script>
                <div id="owl-demo2" class="owl-carousel"> 
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>New</h6></div>
                      <a href="products6.html"><img src="images/p1.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products6.html">Coffee Mug</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$14</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Coffee Mug" /> 
                          <input type="hidden" name="amount" value="14.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>         
                    </div>  
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>20% <br> Off</h6></div>
                      <a href="products6.html"><img src="images/p2.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products6.html">Teddy bear</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$20</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Teddy bear" /> 
                          <input type="hidden" name="amount" value="20.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div> 
                  </div>
                  <div class="item"> 
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>Sale</h6></div>
                      <a href="products6.html"><img src="images/p3.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products6.html">Chocolates</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$60</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Chocolates" /> 
                          <input type="hidden" name="amount" value="60.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div> 
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products6.html"><img src="images/p4.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products6.html">Gift Card</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$22</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Gift Card" /> 
                          <input type="hidden" name="amount" value="22.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>       
                    </div> 
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>New</h6></div>
                      <a href="products6.html"><img src="images/p1.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products6.html">Coffee Mug</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$14</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Coffee Mug" /> 
                          <input type="hidden" name="amount" value="14.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>         
                    </div>  
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>20% <br> Off</h6></div>
                      <a href="products6.html"><img src="images/p2.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products6.html">Teddy bear</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$20</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Teddy bear" /> 
                          <input type="hidden" name="amount" value="20.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div> 
                  </div>
                  <div class="item"> 
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>Sale</h6></div>
                      <a href="products6.html"><img src="images/p3.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products6.html">Chocolates</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$60</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Chocolates" /> 
                          <input type="hidden" name="amount" value="60.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div> 
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products6.html"><img src="images/p4.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products6.html">Gift Card</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$22</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Gift Card" /> 
                          <input type="hidden" name="amount" value="22.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>       
                    </div> 
                  </div> 
                </div>    
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="decor" aria-labelledby="decor-tab">
              <div class="tabcontent-grids">
                <script>
                  $(document).ready(function() { 
                    $("#owl-demo3").owlCarousel({
                   
                      autoPlay: 3000, //Set AutoPlay to 3 seconds
                   
                      items :4,
                      itemsDesktop : [640,5],
                      itemsDesktopSmall : [414,4],
                      navigation : true
                   
                    });
                    
                  }); 
                </script>
                <div id="owl-demo3" class="owl-carousel"> 
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>Sale</h6></div>
                      <a href="products3.html"><img src="images/h1.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products3.html">Wall Clock</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$80</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Wall Clock" /> 
                          <input type="hidden" name="amount" value="80.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>         
                    </div>  
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>10%<br>Off</h6></div>
                      <a href="products3.html"><img src="images/h2.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products3.html">Plants & Vases</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$40</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Plants & Vases" /> 
                          <input type="hidden" name="amount" value="40.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>         
                    </div>
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products3.html"><img src="images/h3.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products3.html">Queen Size Bed</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$250</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Queen Size Bed" /> 
                          <input type="hidden" name="amount" value="250.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div> 
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products3.html"><img src="images/h4.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products3.html">flower pot</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$30</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="flower pot" /> 
                          <input type="hidden" name="amount" value="30.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div> 
                  </div> 
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>Sale</h6></div>
                      <a href="products3.html"><img src="images/h1.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products3.html">Wall Clock</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$80</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Wall Clock" /> 
                          <input type="hidden" name="amount" value="80.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>         
                    </div>  
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>10%<br>Off</h6></div>
                      <a href="products3.html"><img src="images/h2.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products3.html">Plants & Vases</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$40</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Plants & Vases" /> 
                          <input type="hidden" name="amount" value="40.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>         
                    </div>
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products3.html"><img src="images/h3.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products3.html">Queen Size Bed</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$250</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Queen Size Bed" /> 
                          <input type="hidden" name="amount" value="250.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div> 
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products3.html"><img src="images/h4.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products3.html">flower pot</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$30</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="flower pot" /> 
                          <input type="hidden" name="amount" value="30.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div> 
                  </div>  
                </div>    
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="sports" aria-labelledby="sports-tab">
              <div class="tabcontent-grids">
                <script>
                  $(document).ready(function() { 
                    $("#owl-demo4").owlCarousel({
                   
                      autoPlay: 3000, //Set AutoPlay to 3 seconds
                   
                      items :4,
                      itemsDesktop : [640,5],
                      itemsDesktopSmall : [414,4],
                      navigation : true
                   
                    }); 
                  }); 
                </script>
                <div id="owl-demo4" class="owl-carousel"> 
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>New</h6></div>
                      <a href="products4.html"><img src="images/s1.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products4.html">Roller Skates</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$180</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Roller Skates"/> 
                          <input type="hidden" name="amount" value="180.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>         
                    </div>  
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products4.html"><img src="images/s2.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products4.html">Football</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$70</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Football"/> 
                          <input type="hidden" name="amount" value="70.00"/>
                          <button type="submit" class="w3ls-cart"><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div> 
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>20% <br>Off</h6></div>
                      <a href="products4.html"><img src="images/s3.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products4.html">Nylon Shuttle</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$56</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Nylon Shuttle" /> 
                          <input type="hidden" name="amount" value="56.00"/> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>       
                    </div> 
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products4.html"><img src="images/s4.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products4.html">Cricket Kit</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$80</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Cricket Kit" /> 
                          <input type="hidden" name="amount" value="80.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>       
                    </div> 
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>New</h6></div>
                      <a href="products4.html"><img src="images/s1.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products4.html">Roller Skates</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$180</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Roller Skates"/> 
                          <input type="hidden" name="amount" value="180.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>         
                    </div>  
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products4.html"><img src="images/s2.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products4.html">Football</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$70</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Football"/> 
                          <input type="hidden" name="amount" value="70.00"/>
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>        
                    </div> 
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <div class="new-tag"><h6>20% <br>Off</h6></div>
                      <a href="products4.html"><img src="images/s3.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products4.html">Nylon Shuttle</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$56</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Nylon Shuttle" /> 
                          <input type="hidden" name="amount" value="56.00"/> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>       
                    </div> 
                  </div>
                  <div class="item">
                    <div class="glry-w3agile-grids agileits"> 
                      <a href="products4.html"><img src="images/s4.png" alt="img"></a>
                      <div class="view-caption agileits-w3layouts">           
                        <h4><a href="products4.html">Cricket Kit</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur</p>
                        <h5>$80</h5>
                        <form action="#" method="post">
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" /> 
                          <input type="hidden" name="w3ls_item" value="Cricket Kit" /> 
                          <input type="hidden" name="amount" value="80.00" /> 
                          <button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
                        </form>
                      </div>       
                    </div> 
                  </div>
                </div>    
              </div>
            </div> 
          </div>   
        </div>  
      </div>    
    </div>    
  </div> 
  <!-- //welcome -->


	<div class="products">	 
		<div class="container">
			<div class="col-md-9 product-w3ls-right">
				<!-- breadcrumbs --> 
		<!-- 		<ol class="breadcrumb breadcrumb1">
					<li><a href="index.html">Home</a></li>
					<li class="active">Products</li>
				</ol>  -->
				<div class="clearfix"> </div>
				<!-- //breadcrumbs -->
				<div class="product-top">
					<h4 id="categoryName">Fashion Store</h4>
					<ul> 
						<li class="dropdown head-dpdn">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Filter By<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a class="s" href="#filter-by-name" onclick = "return filterByName()">Low price</a></li> 
								<li><a class="s" href="#filter-by-name2" onclick = "return filterByName2()">High price</a></li>
								<li><a >Latest</a></li> 
								<li><a href="#">Popular</a></li> 
							</ul> 
						</li>
					 
						<li class="dropdown head-dpdn">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Brands<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#">Brand1</a></li> 
								<li><a href="#">Brand2</a></li>
								<li><a href="#">Brand3</a></li> 
								<li><a href="#">Brand4</a></li> 
							</ul> 
						</li>
					</ul>  
					<div class="clearfix"> </div>
				</div>
				 <script>


         $(document).ready(function(){


       //  	alert("it works");
         //$('#userTable').html(' loading...');
             /*alert(<?php echo site_url();?>);*/
             //alert("it works");
    /*         var xxx=<?php site_url('Product/jsonData')?>;
             alert(xxx);*/


             
    $.ajax({


        url: "<?php echo site_url('Product/jsonData')?>",
        type: "get",
        dataType: "JSON",
         beforeSend: function() {
              $("#myDiv").show();
           },
        success: function(response){
            //alert("it works");
            $("#myDiv").hide();
            var len = response.length;
            for(var i=0; i<len; i++){

 /*               var id = response[i].eclast_code;
                var username = response[i].name;
                var name = response[i].main_image;
                var email = "email";*/
                var main_image=response[i].main_image;
                var id=response[i].id;
                var eclast_code=response[i].eclast_code;
                var eclast_price=response[i].eclast_price;
                var name=response[i].name;
                var slug=response[i].slug;
                var potongName=name.substring(0, 40);
                        var	number_string = eclast_price.toString(),sisa 	= number_string.length % 3,rupiah 	= number_string.substr(0, sisa),ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
		
if (ribuan) {
	separator = sisa ? '.' : '';
	rupiah += separator + ribuan.join('.');
}


            /*    var tr_str = "<tr>" +
                    "<td align='center'>" + (i+1) + "</td>" +
                    "<td align='center'>" + id + "</td>" +
                    "<td align='center'><img src='/storage/IMG/" +name + ".jpg'/></td>" +
                    "<td align='center'>" + username + "</td>" +
                    "</tr>";
            */   
             var tr_str='<div class="toRemove"><div class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>18%<br>Off</h6></div><a href="single.html">'+'<img src="/storage/tas-wanita/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><h5><a href="/detail/'+id+'-'+slug+'.html">'+potongName+'</a></h5><h6><del>$25</del>Rp '+rupiah+'</h6></div></div></div>';
				

               // $("#produkTampung").append(tr_str);
                $("#produkTampung").append(tr_str);
                window.history.pushState("object or string", "Title", "/products/3-sepatuhk.html");
            }


         }

        });
    });
         function kategoriProduct(param)
         {
          $('.cekbok').prop('checked',false);
          NProgress.configure({ showSpinner: false });
          //NProgress.set(0);
          NProgress.start();
          NProgress.set(0.1);
      
          $.ajax({

        url: "<?php echo site_url('Product/kategoriProduct/')?>"+param,
        type: "get",
        dataType: "JSON",
         beforeSend: function() {
          if(param==1)
          {
          $(".subitem1twn").css('background-color', '#FB4602');
          $(".subitem1twn").css('color', '#FFF');
          $(".subitem1spw").css('background-color', '#FFF');
          $(".subitem1spw").css('color', 'black');
           $(".subitem1jmt").css('background-color', '#FFF');
          $(".subitem1jmt").css('color', 'black');
           $(".subitem1kcm").css('background-color', '#FFF');
          $(".subitem1kcm").css('color', 'black');
          }
          else if(param==2)
          {
          $(".subitem1spw").css('background-color', '#FB4602');
          $(".subitem1spw").css('color', '#FFF');
          $(".subitem1twn").css('background-color', '#FFF');
          $(".subitem1twn").css('color', 'black');
           $(".subitem1jmt").css('background-color', '#FFF');
          $(".subitem1jmt").css('color', 'black');
           $(".subitem1kcm").css('background-color', '#FFF');
          $(".subitem1kcm").css('color', 'black');
          }
           else if(param==3)
          {
          $(".subitem1jmt").css('background-color', '#FB4602');
          $(".subitem1jmt").css('color', '#FFF');
          $(".subitem1twn").css('background-color', '#FFF');
          $(".subitem1twn").css('color', 'black');
           $(".subitem1spw").css('background-color', '#FFF');
          $(".subitem1spw").css('color', 'black');
           $(".subitem1kcm").css('background-color', '#FFF');
          $(".subitem1kcm").css('color', 'black');
          }
          else if(param==4)
          {
          $(".subitem1kcm").css('background-color', '#FB4602');
          $(".subitem1kcm").css('color', '#FFF');
          $(".subitem1twn").css('background-color', '#FFF');
          $(".subitem1twn").css('color', 'black');
           $(".subitem1spw").css('background-color', '#FFF');
          $(".subitem1spw").css('color', 'black');
           $(".subitem1jmt").css('background-color', '#FFF');
          $(".subitem1jmt").css('color', 'black');
          }
              //$("#myDiv").show();
                  NProgress.set(0.5);
      
           },
        success: function(response){
          $(".toRemove").remove();
          NProgress.done();
            //alert("it works");
            //$("#myDiv").hide();

            var len = response.length;
            for(var i=0; i<len; i++){
 /*               var id = response[i].eclast_code;
                var username = response[i].name;
                var name = response[i].main_image;
                var email = "email";*/
                var main_image=response[i].main_image;
                var id=response[i].id;
                var eclast_code=response[i].eclast_code;
                var eclast_price=response[i].eclast_price;
                var name=response[i].name;
                var slug=response[i].slug;
                var id_category=response[i].id_category;
                var folder_img;
                var url_cat;
                if(id_category == 2)
                {
                  folder_image="sepatu-wanita";
                  url_cat ='/produk/2-sepatu-wanita-bata.html';

                }
                else if(id_category == 1)
                {
                  folder_image="tas-wanita";
                  url_cat ='/produk/1-tas-wanita-sophie-martin.html';
                }
                  else if(id_category == 3)
                {
                  folder_image="jam-tangan";
                  url_cat ='/produk/3-jam-tangan-garmin.html';
                }
                 else if(id_category == 4)
                {
                  folder_image="kaca-mata";
                  url_cat ='/produk/4-kaca-mata-owl.html';
                }

                //alert(id_category);
                var potongName=name.substring(0, 35);
                        var number_string = eclast_price.toString(),sisa  = number_string.length % 3,rupiah   = number_string.substr(0, sisa),ribuan  = number_string.substr(sisa).match(/\d{3}/g);
    
if (ribuan) {
  separator = sisa ? '.' : '';
  rupiah += separator + ribuan.join('.');
}


            /*    var tr_str = "<tr>" +
                    "<td align='center'>" + (i+1) + "</td>" +
                    "<td align='center'>" + id + "</td>" +
                    "<td align='center'><img src='/storage/IMG/" +name + ".jpg'/></td>" +
                    "<td align='center'>" + username + "</td>" +
                    "</tr>";
            */   
             var tr_str='<div class="toRemove"><div class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>18%<br>Off</h6></div><a href="single.html">'+'<img src="/storage/'+folder_image+'/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><h5><a href="/detail/'+id+'-'+slug+'.html">'+potongName+'</a></h5><h6><del>$25</del>Rp '+rupiah+'</h6><form action="#" method="post"><input type="hidden" name="cmd" value="_cart" /><input type="hidden" name="add" value="1" /> <input type="hidden" name="w3ls_item" value="Leather Jacket" /><input type="hidden" name="amount" value="20.00" /><button class="w3ls-cart pw3ls-cart"><i class="fa fa-check-square-o" aria-hidden="true"></i><a style="color:inherit" href="/detail/'+id+'-'+slug+'.html"> Lihat Detail</a></button> </form></div></div></div>';
        

               // $("#produkTampung").append(tr_str);
                $("#produkTampung").append(tr_str);
                $('#categoryName').text("Sepatu Nike");
                window.history.pushState("object or string", "Title", url_cat);
            }


         }

        });
          //alert("it works");

         }

   
         function filterByName()
         {
         	
         	//$("#produkTampung").hide();
         	
         	//alert("test");
         	//alert("filterByName");
			NProgress.configure({ showSpinner: false });
         	//NProgress.set(0);
         	NProgress.start();
         	NProgress.set(0.1);
			
         	
         
         	    $.ajax({

        url: "<?php echo site_url('Product/jsonData2')?>",
        type: "get",
        dataType: "JSON",
         beforeSend: function() {
         
         	NProgress.set(0.5);
         	
              //$("#myDiv").show();
              
           },
        success: function(response){
        	NProgress.done();


         	
            //alert("it works");
            //$("#myDiv").hide();
            $(".toRemove").remove();

            var len = response.length;
            for(var i=0; i<len; i++){
 /*               var id = response[i].eclast_code;
                var username = response[i].name;
                var name = response[i].main_image;
                var email = "email";*/
                var main_image=response[i].main_image;
                var id=response[i].id;
                var eclast_code=response[i].name;
                var eclast_price=response[i].eclast_price;
                var slug=response[i].slug;

                var	number_string = eclast_price.toString(),sisa 	= number_string.length % 3,rupiah 	= number_string.substr(0, sisa),ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
		
if (ribuan) {
	separator = sisa ? '.' : '';
	rupiah += separator + ribuan.join('.');
}


            /*    var tr_str = "<tr>" +
                    "<td align='center'>" + (i+1) + "</td>" +
                    "<td align='center'>" + id + "</td>" +
                    "<td align='center'><img src='/storage/IMG/" +name + ".jpg'/></td>" +
                    "<td align='center'>" + username + "</td>" +
                    "</tr>";
            */    var tr_str='<div class="toRemove"><div  class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>18%<br>Off</h6></div><a href="single.html">'+'<img src="/storage/IMG/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><p><a href="/detail/'+id+'-'+slug+'.html">'+eclast_code+'</a></p><h6><del>$25</del>Rp '+rupiah+'</h6><form action="#" method="post"><input type="hidden" name="cmd" value="_cart" /><input type="hidden" name="add" value="1" /> <input type="hidden" name="w3ls_item" value="Leather Jacket" /><input type="hidden" name="amount" value="20.00" /><button class="w3ls-cart pw3ls-cart"><i class="fa fa-check-square-o" aria-hidden="true"></i><a style="color:inherit" href="/detail/'+id+'-'+slug+'.html"> Lihat Detail</a></button></form> </div></div></div>';
				
                $("#myDiv").hide();
                

                $("#produkTampung").append(tr_str);
				  window.history.pushState("object or string", "Title", "/produk/3-sepatu.html?filter-by=popular");
            }
         

         }

        });
         }

         function filterByName2()
         {
         	//$("#produkTampung").hide();
         	
         	//alert("test");
         	//alert("filterByName");
         	    $.ajax({

        url: "<?php echo site_url('Product/jsonData')?>",
        type: "get",
        dataType: "JSON",
         beforeSend: function() {
              //$("#myDiv").show();

           },
        success: function(response){
        	$(".toRemove").remove();
            //alert("it works");
            //$("#myDiv").hide();

            var len = response.length;
            for(var i=0; i<len; i++){
 /*               var id = response[i].eclast_code;
                var username = response[i].name;
                var name = response[i].main_image;
                var email = "email";*/
                var main_image=response[i].main_image;
                var id=response[i].id;
                var eclast_code=response[i].name;
                var eclast_price=response[i].eclast_price;
                var slug=response[i].slug;

                var	number_string = eclast_price.toString(),sisa 	= number_string.length % 3,rupiah 	= number_string.substr(0, sisa),ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
		
if (ribuan) {
	separator = sisa ? '.' : '';
	rupiah += separator + ribuan.join('.');
}
            /*    var tr_str = "<tr>" +
                    "<td align='center'>" + (i+1) + "</td>" +
                    "<td align='center'>" + id + "</td>" +
                    "<td align='center'><img src='/storage/IMG/" +name + ".jpg'/></td>" +
                    "<td align='center'>" + username + "</td>" +
                    "</tr>";
            */    var tr_str='<div class="toRemove"><div class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>18%<br>Off</h6></div><a href="single.html">'+'<img src="/storage/IMG/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><p><a href="/detail/'+id+'-'+slug+'.html">'+eclast_code+'</a></p><h6><del>$25</del>Rp '+rupiah+'</h6></div></div></div>';
                $("#myDiv").hide();
                $("#produkTampung").append(tr_str);
				  window.history.pushState("object or string", "Title", "/produk/3-handphone.html?filter-by=harga");
            }
         }
        });
         }
</script>
				<div class="products-row">
					            <div id="myDiv" >
         <!-- <img id="loading-image" src="/storage/SYSTEM_IMG/Eclipse.gif"/> --> 
        <br/><br/>
        <div class="marquee">
        <center><h3> L o a d i n g . . .</h3></center></div>
    </div>
					<div id="produkTampung">
					</div>
					<div id="produkTampung2">
					</div>
						
					<div class="clearfix"> </div>
				

				</div>
				<!-- add-products --> 
<!-- 				<div class="w3ls-add-grids w3agile-add-products">
					<a href="#"> 
						<h4>TOP 10 TRENDS FOR YOU FLAT <span>20%</span> OFF</h4>
						<h6>Shop now <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></h6>
					</a>
				</div>  -->
				<!-- //add-products -->
			</div>
			<script>
		$(document).ready(function() {
        $(".cekbok").click(function(){

         var url      = window.location.href;

          var arr=url.split('/');
          var arr2=arr[4].split('-');

          var kategoriUrl=arr2[0];
          //alert(arr2[0]);
        
        	NProgress.configure({ showSpinner: false });
          //NProgress.set(0);
          NProgress.start();
          NProgress.set(0.1);
            var pilih = [];
            var q;
            var param ;
            $.each($("input[name='harga']:checked"), function(){         
                pilih.push($(this).val());
            });
            param=pilih.join("_");
            //alert("Anda memilih range harga: " +param );
            var url="<?php echo site_url('Product/sort_byPrice/')?>"+kategoriUrl+"/"+param;
          //alert(url);

             $.ajax({

        url: url,
        type: "get",
        dataType: "JSON",
         beforeSend: function() {
              NProgress.set(0.5);
      
          //alert("sukses");
            
           },
        success: function(response){

          var filtered = response.filter(function(item) { 
            return item.id_category == kategoriUrl;  
          });


           
            $(".toRemove").remove();
            NProgress.done();

            var len = filtered.length;
            for(var i=0; i<len; i++){
             /* var xxx=response[i].paramX;
              alert(xxx);
          */      var main_image=filtered[i].main_image;
                var id=filtered[i].id;
                var eclast_code=filtered[i].eclast_code;
                var eclast_price=filtered[i].eclast_price;
                var name=filtered[i].name;
                var slug=filtered[i].slug;
                var id_category=response[i].id_category;
                var potongName=name.substring(0, 35);
                        var number_string = eclast_price.toString(),sisa  = number_string.length % 3,rupiah   = number_string.substr(0, sisa),ribuan  = number_string.substr(sisa).match(/\d{3}/g);
    
if (ribuan) {
  separator = sisa ? '.' : '';
  rupiah += separator + ribuan.join('.');
}
if(id_category == 2)
                {
                  folder_image="sepatu-wanita";
                  url_cat ='/produk/2-sepatu-wanita-bata.html';

                }
                else if(id_category == 1)
                {
                  folder_image="tas-wanita";
                  url_cat ='/produk/1-tas-wanita-sophie-martin.html';
                }
                  else if(id_category == 3)
                {
                  folder_image="jam-tangan";
                  url_cat ='/produk/3-jam-tangan-garmin.html';
                }
                 else if(id_category == 4)
                {
                  folder_image="kaca-mata";
                  url_cat ='/produk/4-kaca-mata-owl.html';
                }


            /* var tr_str='<div class="toRemove"><div class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>18%<br>Off</h6></div><a href="single.html">'+'<img src="/storage/'+folder_image+'/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><h5><a href="/detail/'+id+'-'+slug+'.html">'+potongName+'</a></h5><h6><del>'+(rupiah*120)/100+'0</del>Rp '+rupiah+'</h6></div></div></div>';*/
              var tr_str='<div class="toRemove"><div class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>18%<br>Off</h6></div><a href="single.html">'+'<img src="/storage/'+folder_image+'/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><h5><a href="/detail/'+id+'-'+slug+'.html">'+potongName+'</a></h5><h6><del>$25</del>Rp '+rupiah+'</h6><form action="#" method="post"><input type="hidden" name="cmd" value="_cart" /><input type="hidden" name="add" value="1" /> <input type="hidden" name="w3ls_item" value="Leather Jacket" /><input type="hidden" name="amount" value="20.00" /><button class="w3ls-cart pw3ls-cart"><i class="fa fa-check-square-o" aria-hidden="true"></i><a style="color:inherit" href="/detail/'+id+'-'+slug+'.html"> Lihat Detail</a></button> </form></div></div></div>';
        
                $("#produkTampung").append(tr_str);
/*                window.history.pushState("object or string", "Title", "/products/3-sepatuh.html");*/
            }


         }
		});



        });
        });
			</script>
			<div class="col-md-3 rsidebar">
				<div class="rsidebar-top">
					<div class="slider-left">
						<h4>Filter Harga</h4>            
						<div class="row row1 scroll-pane">
							<label class="checkbox"><input type="checkbox" value="0-100000" name="harga" class="cekbok"><i></i>0 - 100.000 </label>
							<label class="checkbox"><input type="checkbox" name="harga" value="100000-200000"class="cekbok" ><i></i>100.000 - 200.000 </label> 
							<label class="checkbox"><input type="checkbox" name="harga" value="200000-500000"class="cekbok"><i></i>200.000 - 500.000  </label> 
							<label class="checkbox"><input type="checkbox" name="harga" value="500000-1000000"class="cekbok"><i></i>500.000 - 1.000.000 </label> 
							<label class="checkbox"><input type="checkbox" name="harga" value="1000000-2000000"class="cekbok"><i></i>1.000.000 - 2.000.000 </label> 
													</div> 
					</div>
					<div class="sidebar-row">
						<h4>Kategori</h4>
						<ul class="faq">
							<li class="item1"><a href="#">Tas Wanita<span class="glyphicon glyphicon-menu-down"></span></a>
								<ul>
									<li class="subitem1twn"><a href="#" class="subitem1twn" onclick = "return kategoriProduct(1)">Sophie Martin</a></li>										
									<li class="subitem1"><a href="#">Bonia</a></li>										
									<li class="subitem1"><a href="#">Ellen</a></li>										
									<li class="subitem1"><a href="#">LVH</a></li>										
								</ul>
							</li>
							<li class="item2"><a href="#">Jam Tangan<span class="glyphicon glyphicon-menu-down"></span></a>
								<ul>
									<li class="subitem1jmt"><a  class="subitem1jmt" href="#" onclick = "return kategoriProduct(3)">Garmin</a></li>										
									<li class="subitem2jmt"><a href="#">Eyewear</a></li>			 										
									<li class="subitem1"><a href="#">Jewellery</a></li>										
									<li class="subitem1"><a href="#">Footwear</a></li>										
								</ul>
							</li>
							<li class="item3"><a href="#">Sepatu Wanita<span class="glyphicon glyphicon-menu-down"></span></a>
								<ul>
									<li class="subitem1spw"><a class="subitem1spw" href="#" onclick = "return kategoriProduct(2)">Bata</a></li>
									<li class="subitem1"><a href="#">Adidas</a></li>														
								</ul>
							</li>

                <li class="item3"><a href="#">Kaca Mata<span class="glyphicon glyphicon-menu-down"></span></a>
                <ul>
                  <li class="subitem1kcm"><a class="subitem1kcm" href="#" onclick = "return kategoriProduct(4)">OWL</a></li>
                  <li class="subitem1"><a href="#">Adidas</a></li>                            
                </ul>
              </li>
						</ul>
						<!-- script for tabs -->
						<script type="text/javascript">
							$(function() {
							
								var menu_ul = $('.faq > li > ul'),
									   menu_a  = $('.faq > li > a');
								
								menu_ul.show();
							
								menu_a.click(function(e) {
									e.preventDefault();
									if(!$(this).hasClass('active')) {
										menu_a.removeClass('active');
										menu_ul.filter(':visible').slideUp('normal');
										$(this).addClass('active').next().stop(true,true).slideDown('normal');
									} else {
										$(this).removeClass('active');
										$(this).next().stop(true,true).slideUp('normal');
									}
								});
							
							});
						</script>
						<!-- script for tabs -->
					</div>
					<div class="sidebar-row">
						<h4>DISCOUNTS</h4>
						<div class="row row1 scroll-pane">
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Upto - 10% (20)</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>70% - 60% (5)</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>50% - 40% (7)</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>30% - 20% (2)</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>10% - 5% (5)</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>30% - 20% (7)</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>10% - 5% (2)</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Other(50)</label>
						</div>
					</div>
					<div class="sidebar-row">
						<h4>Color</h4>
						<div class="row row1 scroll-pane">
							<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>White</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Pink</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Gold</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Blue</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Orange</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i> Brown</label> 
						</div>
					</div>			 
				</div>
				<div class="related-row">
					<h4>Related Searches</h4>
					<ul>
						<li><a href="products.html">Travel Bags</a></li>
						<li><a href="products.html">Casual Wear</a></li>
						<li><a href="products.html">Beauty Gift Hampers</a></li>
						<li><a href="products.html">T-Shirts</a></li>
						<li><a href="products.html">Blazers</a></li>
						<li><a href="products.html">Parkas</a></li>
						<li><a href="products.html">Shoes</a></li>
						<li><a href="products.html">Hair Care</a></li>
						<li><a href="products.html">Bath & Spa</a></li>
						<li><a href="products.html">Handbags</a></li>
					</ul>
				</div>
				<div class="related-row">
					<h4>YOU MAY ALSO LIKE</h4>
					<div class="galry-like">  
						<a href="single.html"><img src="images/e1.png" class="img-responsive" alt="img"></a>             
						<h4><a href="products.html">Audio speaker</a></h4> 
						<h5>$100</h5>       
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
			<!-- recommendations -->
			<div class="recommend">
				<h3 class="w3ls-title">Our Recommendations </h3> 
				<script>
					$(document).ready(function() { 
						$("#owl-demo5").owlCarousel({
					 
						  autoPlay: 3000, //Set AutoPlay to 3 seconds
					 
						  items :4,
						  itemsDesktop : [640,5],
						  itemsDesktopSmall : [414,4],
						  navigation : true
					 
						});
						
					}); 
				</script>
				<div id="owl-demo5" class="owl-carousel">
					<div class="item">
						<div class="glry-w3agile-grids agileits">
							<div class="new-tag"><h6>20% <br> Off</h6></div>
							<a href="products1.html"><img src="images/f2.png" alt="img"></a>
							<div class="view-caption agileits-w3layouts">           
								<h4><a href="products1.html">Women Sandal</a></h4>
								<p>Lorem ipsum dolor sit amet consectetur</p>
								<h5>$20</h5>
								<form action="#" method="post">
									<input type="hidden" name="cmd" value="_cart" />
									<input type="hidden" name="add" value="1" /> 
									<input type="hidden" name="w3ls_item" value="Women Sandal" /> 
									<input type="hidden" name="amount" value="20.00" /> 
									<button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
								</form>
							</div>        
						</div> 
					</div>
					<div class="item">
						<div class="glry-w3agile-grids agileits"> 
							<a href="products.html"><img src="images/e4.png" alt="img"></a>
							<div class="view-caption agileits-w3layouts">           
								<h4><a href="products.html">Digital Camera</a></h4>
								<p>Lorem ipsum dolor sit amet consectetur</p>
								<h5>$80</h5>
								<form action="#" method="post">
									<input type="hidden" name="cmd" value="_cart" />
									<input type="hidden" name="add" value="1" /> 
									<input type="hidden" name="w3ls_item" value="Digital Camera"/> 
									<input type="hidden" name="amount" value="100.00" /> 
									<button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
								</form>
							</div>         
						</div>  
					</div>  
					<div class="item">
						<div class="glry-w3agile-grids agileits"> 
							<div class="new-tag"><h6>New</h6></div>
							<a href="products4.html"><img src="images/s1.png" alt="img"></a>
							<div class="view-caption agileits-w3layouts">           
								<h4><a href="products4.html">Roller Skates</a></h4>
								<p>Lorem ipsum dolor sit amet consectetur</p>
								<h5>$180</h5>
								<form action="#" method="post">
									<input type="hidden" name="cmd" value="_cart" />
									<input type="hidden" name="add" value="1" /> 
									<input type="hidden" name="w3ls_item" value="Roller Skates"/> 
									<input type="hidden" name="amount" value="180.00" /> 
									<button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
								</form>
							</div>         
						</div>  
					</div>
					<div class="item">
						<div class="glry-w3agile-grids agileits"> 
							<a href="products1.html"><img src="images/f1.png" alt="img"></a>
							<div class="view-caption agileits-w3layouts">           
								<h4><a href="products1.html">T Shirt</a></h4>
								<p>Lorem ipsum dolor sit amet consectetur</p>
								<h5>$10</h5>
								<form action="#" method="post">
									<input type="hidden" name="cmd" value="_cart" />
									<input type="hidden" name="add" value="1" /> 
									<input type="hidden" name="w3ls_item" value="T Shirt" /> 
									<input type="hidden" name="amount" value="10.00" /> 
									<button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
								</form>
							</div>        
						</div>    
					</div>
					<div class="item">
						<div class="glry-w3agile-grids agileits"> 
							<div class="new-tag"><h6>New</h6></div>
							<a href="products6.html"><img src="images/p1.png" alt="img"></a>
							<div class="view-caption agileits-w3layouts">           
								<h4><a href="products6.html">Coffee Mug</a></h4>
								<p>Lorem ipsum dolor sit amet consectetur</p>
								<h5>$14</h5>
								<form action="#" method="post">
									<input type="hidden" name="cmd" value="_cart" />
									<input type="hidden" name="add" value="1" /> 
									<input type="hidden" name="w3ls_item" value="Coffee Mug" /> 
									<input type="hidden" name="amount" value="14.00" /> 
									<button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
								</form>
							</div>         
						</div>  
					</div>
					<div class="item">
						<div class="glry-w3agile-grids agileits"> 
							<div class="new-tag"><h6>20% <br> Off</h6></div>
							<a href="products6.html"><img src="images/p2.png" alt="img"></a>
							<div class="view-caption agileits-w3layouts">           
								<h4><a href="products6.html">Teddy bear</a></h4>
								<p>Lorem ipsum dolor sit amet consectetur</p>
								<h5>$20</h5>
								<form action="#" method="post">
									<input type="hidden" name="cmd" value="_cart" />
									<input type="hidden" name="add" value="1" /> 
									<input type="hidden" name="w3ls_item" value="Teddy bear" /> 
									<input type="hidden" name="amount" value="20.00" /> 
									<button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
								</form>
							</div>        
						</div> 
					</div>
					<div class="item">
						<div class="glry-w3agile-grids agileits"> 
							<a href="products4.html"><img src="images/s2.png" alt="img"></a>
							<div class="view-caption agileits-w3layouts">           
								<h4><a href="products4.html">Football</a></h4>
								<p>Lorem ipsum dolor sit amet consectetur</p>
								<h5>$70</h5>
								<form action="#" method="post">
									<input type="hidden" name="cmd" value="_cart" />
									<input type="hidden" name="add" value="1" /> 
									<input type="hidden" name="w3ls_item" value="Football"/> 
									<input type="hidden" name="amount" value="70.00"/>
									<button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
								</form>
							</div>        
						</div> 
					</div> 
					<div class="item">
						<div class="glry-w3agile-grids agileits"> 
							<div class="new-tag"><h6>Sale</h6></div>
							<a href="products3.html"><img src="images/h1.png" alt="img"></a>
							<div class="view-caption agileits-w3layouts">           
								<h4><a href="products3.html">Wall Clock</a></h4>
								<p>Lorem ipsum dolor sit amet consectetur</p>
								<h5>$80</h5>
								<form action="#" method="post">
									<input type="hidden" name="cmd" value="_cart" />
									<input type="hidden" name="add" value="1" /> 
									<input type="hidden" name="w3ls_item" value="Wall Clock" /> 
									<input type="hidden" name="amount" value="80.00" /> 
									<button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
								</form>
							</div>         
						</div>  
					</div> 
				</div>    
			</div>
			<!-- //recommendations -->
		</div>
	</div>
	<!--//products-->  
	<!-- footer-top -->
	<div class="w3agile-ftr-top">
		<div class="container">
			<div class="ftr-toprow">
				<div class="col-md-4 ftr-top-grids">
					<div class="ftr-top-left">
						<i class="fa fa-truck" aria-hidden="true"></i>
					</div> 
					<div class="ftr-top-right">
						<h4>FREE DELIVERY</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
					</div> 
					<div class="clearfix"> </div>
				</div> 
				<div class="col-md-4 ftr-top-grids">
					<div class="ftr-top-left">
						<i class="fa fa-user" aria-hidden="true"></i>
					</div> 
					<div class="ftr-top-right">
						<h4>CUSTOMER CARE</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
					</div> 
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-4 ftr-top-grids">
					<div class="ftr-top-left">
						<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
					</div> 
					<div class="ftr-top-right">
						<h4>GOOD QUALITY</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
					</div>
					<div class="clearfix"> </div>
				</div> 
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //footer-top --> 
	<!-- subscribe -->
	<div class="subscribe"> 
		<div class="container">
			<div class="col-md-6 social-icons w3-agile-icons">
				<h4>Keep in touch</h4>  
				<ul>
					<li><a href="#" class="fa fa-facebook icon facebook"> </a></li>
					<li><a href="#" class="fa fa-twitter icon twitter"> </a></li>
					<li><a href="#" class="fa fa-google-plus icon googleplus"> </a></li>
					<li><a href="#" class="fa fa-dribbble icon dribbble"> </a></li>
					<li><a href="#" class="fa fa-rss icon rss"> </a></li> 
				</ul>
				<ul class="apps"> 
					<li><h4>Download Our app : </h4> </li>
					<li><a href="#" class="fa fa-apple"></a></li>
					<li><a href="#" class="fa fa-windows"></a></li>
					<li><a href="#" class="fa fa-android"></a></li>
				</ul>
			</div> 
			<div class="col-md-6 subscribe-right">
				<h4>Sign up for email and get 25%off!</h4> 
				<form action="#" method="post"> 
					<input type="text" name="email" placeholder="Enter your Email..." required="">
					<input type="submit" value="Subscribe">
				</form>
				<div class="clearfix"> </div> 
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //subscribe --> 
	<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="footer-info w3-agileits-info">
				<div class="col-md-4 address-left agileinfo">
					<div class="footer-logo header-logo">
						<h2><a href="index.html"><span>S</span>mart <i>Bazaar</i></a></h2>
						<h6>Your stores. Your place.</h6>
					</div>
					<ul>
						<li><i class="fa fa-map-marker"></i> 123 San Sebastian, New York City USA.</li>
						<li><i class="fa fa-mobile"></i> 333 222 3333 </li>
						<li><i class="fa fa-phone"></i> +222 11 4444 </li>
						<li><i class="fa fa-envelope-o"></i> <a href="mailto:example@mail.com"> mail@example.com</a></li>
					</ul> 
				</div>
				<div class="col-md-8 address-right">
					<div class="col-md-4 footer-grids">
						<h3>Company</h3>
						<ul>
							<li><a href="about.html">About Us</a></li>
							<li><a href="marketplace.html">Marketplace</a></li>  
							<li><a href="values.html">Core Values</a></li>  
							<li><a href="privacy.html">Privacy Policy</a></li>
						</ul>
					</div>
					<div class="col-md-4 footer-grids">
						<h3>Services</h3>
						<ul>
							<li><a href="contact.html">Contact Us</a></li>
							<li><a href="login.html">Returns</a></li> 
							<li><a href="faq.html">FAQ</a></li>
							<li><a href="sitemap.html">Site Map</a></li>
							<li><a href="login.html">Order Status</a></li>
						</ul> 
					</div>
					<div class="col-md-4 footer-grids">
						<h3>Payment Methods</h3>
						<ul>
							<li><i class="fa fa-laptop" aria-hidden="true"></i> Net Banking</li>
							<li><i class="fa fa-money" aria-hidden="true"></i> Cash On Delivery</li>
							<li><i class="fa fa-pie-chart" aria-hidden="true"></i>EMI Conversion</li>
							<li><i class="fa fa-gift" aria-hidden="true"></i> E-Gift Voucher</li>
							<li><i class="fa fa-credit-card" aria-hidden="true"></i> Debit/Credit Card</li>
						</ul>  
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- //footer -->
	<div class="copy-right"> 
		<div class="container">
			<p>© 2018 Eclast Store . All rights reserved | Design by <a href="http://w3layouts.com"> W3layouts.</a></p>
		</div>
	</div> 
	
	<!-- cart-js -->
	<script src="/asset/js/minicart.js"></script>
	<script>
        w3ls.render();

        w3ls.cart.on('w3sb_checkout', function (evt) {
        	var items, len, i;

        	if (this.subtotal() > 0) {
        		items = this.items();

        		for (i = 0, len = items.length; i < len; i++) {
        			items[i].set('shipping', 0);
        			items[i].set('shipping2', 0);
        		}
        	}
        });
    </script>  
	<!-- //cart-js -->  
	<!-- menu js aim -->
	<script src="/asset/js/jquery.menu-aim.js"> </script>
	<script src="/asset/js/main.js"></script> <!-- Resource jQuery -->
	<!-- //menu js aim --> 
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/asset/js/bootstrap.js"></script>
</body>
</html>