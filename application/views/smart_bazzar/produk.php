
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->


<!DOCTYPE html>
<html lang="en">
<head>
<title>Eclast Store | New Look & Always Bold</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Smart Bazaar Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
    function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="/asset/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="/asset/css/style.css" rel="stylesheet" type="text/css" media="all" /> 
<link href="/asset/css/menu.css" rel="stylesheet" type="text/css" media="all" />
<link href="/asset/css/loadingbar.css" rel="stylesheet" type="text/css" media="all" />
<link href="/asset/css/nprogress.css" rel="stylesheet" type="text/css" media="all" />

 <!-- menu style -->  
<link href="/asset/css/animate.min.css" rel="stylesheet" type="text/css" media="all" />   
<link href="/asset/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all"> <!-- carousel slider -->  

<!-- //Custom Theme files -->
<!-- font-awesome icons -->
<link href="/asset/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="/asset/js/jquery-2.2.3.min.js"></script> 
<script src="/asset/js/jquery.loadingbar.js"></script> 
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@1.6.0/src/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@1.6.0/extras/loadingoverlay_progress/loadingoverlay_progress.min.js"></script>
<script src="/asset/js/nprogress.js"></script> 

<script src="/asset/js/owl.carousel.js"></script>
<!-- //js --> 
<!-- web-fonts -->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lovers+Quarrel' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Risque' rel='stylesheet' type='text/css'>

<link href='//fonts.googleapis.com/css?family=Offside' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Tangerine:400,700' rel='stylesheet' type='text/css'>
<!-- web-fonts --> 
<!-- scroll to fixed--> 
<script src="/asset/js/jquery-scrolltofixed-min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
         $(window).scroll(function() {
    if($(window).scrollTop() == $(document).height() - $(window).height()) {
           alert("load more");
    }
});






        // Dock the header to the top of the window when scrolled past the banner. This is the default behaviour.

        $('.header-two').scrollToFixed();  
        // previous summary up the page.

        var summaries = $('.summary');
        summaries.each(function(i) {
            var summary = $(summaries[i]);
            var next = summaries[i + 1];

            summary.scrollToFixed({
                marginTop: $('.header-two').outerHeight(true) + 10, 
                zIndex: 999
            });
        });
    });
</script>
<!-- //scroll to fixed--> 
<!-- start-smooth-scrolling -->
<script type="text/javascript" src="/asset/js/move-top.js"></script>
<script type="text/javascript" src="/asset/js/easing.js"></script>  
<script type="text/javascript">
    jQuery(document).ready(function($) {
      $(".scroll").click(function(event){   
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
      });
    });
</script>
<!-- //end-smooth-scrolling -->
<!-- smooth-scrolling-of-move-up -->
<script type="text/javascript">
  $(document).ready(function() {
  
    var defaults = {
      containerID: 'toTop', // fading element id
      containerHoverID: 'toTopHover', // fading element hover id
      scrollSpeed: 1200,
      easingType: 'linear' 
    };
    
    $().UItoTop({ easingType: 'easeOutQuart' });
    
  });
</script>
<!-- //smooth-scrolling-of-move-up -->  
<!-- the jScrollPane script -->

<script type="text/javascript" src="/asset/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" id="sourcecode">
  $(function()
  {
    $('.scroll-pane').jScrollPane();
  });
</script>
<!-- //the jScrollPane script -->
<script type="text/javascript" src="/asset/js/jquery.mousewheel.js"></script>
<!-- the mousewheel plugin --> 
</head><body>


  <!-- header -->
  <div class="header" >
    <div class="w3ls-header" style="background-color:black;"><!--header-one--> 
      <div class="w3ls-header-left">
        <p><a href="#">Jam kerja : Senin - Jumat | 08.00 - 21.00 </a></p>
      </div>
      <div class="w3ls-header-right">
        <ul>

         <li class="dropdown head-dpdn">
            <a href="help.html" class="dropdown-toggle"><i class="fa fa-facebook" aria-hidden="true"></i> eclaststore</a>
          </li> 
         <li class="dropdown head-dpdn">
            <a href="help.html" class="dropdown-toggle"><i class="fa fa-whatsapp" aria-hidden="true"></i> 081-385-800-400</a>
          </li> 

          <li class="dropdown head-dpdn">
            <a href="card.html" class="dropdown-toggle"><i class="fa fa-instagram" aria-hidden="true"></i> eclaststore</a>
          </li>
          <li class="dropdown head-dpdn">
            <a href="contact.html" class="dropdown-toggle"><i class="fa fa-envelope" aria-hidden="true"></i> bantuan@eclast-store.com</a>
          </li>  
         
        </ul>
      </div>


      <div class="clearfix"> </div> 
    </div>

    <div class="header-two"><!-- header-two -->
      <div class="container">
         <div class="header-logo">
          <h1><a href="/"><span style="">E</span>clast<i><sub>_store.com</sub></i></a></h1>
          <h6>New Look & Always Bold.</h6> 
        </div> 
        <div class="header-search" style="">
          <form action="#" method="post">
            <input type="search" name="Search" placeholder="Cari aja disini..." required="">
            <button type="submit" class="btn btn-default" aria-label="Left Align">
              <i class="fa fa-search" aria-hidden="true"> </i>
            </button>
          </form>
        </div>
        <div class="header-cart" style="">
          <div class="row">
         <div class="col-md-12">
           <div class="row">

          <div class="col-md-12">
            <center><h3> Customer Services</h3></center>
          </div>
          <div class="col-md-12" style="height:5px">
  
          </div>

          <div class="col-md-6">
           <center><h4 style="color:white;background-color:green"><i class="fa fa-whatsapp" aria-hidden="true" style="color:white;background-color:green"></i> 081 385 800 400</h4></center>
          </div>
          <div class="col-md-6">
            <center><h4 style="color:black;background-color:yellow"><i class="fa fa-wechat" aria-hidden="true" style="color:black;background-color:yellow"></i> @eclaststore</h4></center>
          </div>


        </div>



         </div>
       </div>
          <div class="clearfix" > </div> 
        </div> 

        <div class="clearfix"> </div>
      </div>    
    </div><!-- //header-two -->
    

                  <div id="top-merch"></div> 
<?php
include('include/menu.php');
?>


  <!-- //header -->   
  <!-- products -->

<!-- welcome -->
  
  <!-- //welcome -->


  <div class="products" >  
    <div class="container">
      <div class="col-md-9 product-w3ls-right">
        <!-- breadcrumbs --> 
    <!--    <ol class="breadcrumb breadcrumb1">
          <li><a href="index.html">Home</a></li>
          <li class="active">Products</li>
        </ol>  -->
        <div class="clearfix"> </div>
        <!-- //breadcrumbs -->
        <div class="product-top" style="background-color:black;">
          <h4 id="categoryName" style="font-family:arial;">Fashion Store</h4>
          <ul> 

           
            <li class="dropdown head-dpdn">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Brands<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <?php

                   foreach ($kategoriTasWanita->result() as $baris)
                  {
                     $slugKategori=$baris->id."-".$baris->cat_slug."/".$baris->merek_slug.".html";

                  ?>
                   <li><a href=""#top-merch" onclick ="return kategoriProduct(<?=$baris->id;?>,'<?=$slugKategori;?>')"><?php echo $baris->merek;//$slugKategori//$baris->merek;?></a></li> 

                  <?php
                  }

                  ?>  
  
              </ul> 
            </li>
          </ul>  
          <div class="clearfix"> </div>
        </div>
         <script>


         $(document).ready(function(){


       //   alert("it works");
         //$('#userTable').html(' loading...');
             /*alert(<?php echo site_url();?>);*/
             //alert("it works");
    /*         var xxx=<?php site_url('Product/jsonData')?>;
             alert(xxx);*/


          var url      = window.location.href;


          var arr=url.split('/');


          var arr2=arr[4].split('-');



          var param=arr2[0];



             
    $.ajax({


        url: "<?php echo site_url('Product/jsonData/')?>"+param,
        type: "get",
        dataType: "JSON",
              beforeSend: function() {
          //$(".toRemove").remove();
          if(param==1)
          {
          $(".subitem1twn").css('background-color', '#FB4602');
          $(".subitem1twn").css('color', '#FFF');
          $(".subitem1spw").css('background-color', '#FFF');
          $(".subitem1spw").css('color', 'black');
           $(".subitem1jmt").css('background-color', '#FFF');
          $(".subitem1jmt").css('color', 'black');
           $(".subitem1kcm").css('background-color', '#FFF');
          $(".subitem1kcm").css('color', 'black');
          }
          else if(param==2)
          {
          $(".subitem1spw").css('background-color', '#FB4602');
          $(".subitem1spw").css('color', '#FFF');
          $(".subitem1twn").css('background-color', '#FFF');
          $(".subitem1twn").css('color', 'black');
           $(".subitem1jmt").css('background-color', '#FFF');
          $(".subitem1jmt").css('color', 'black');
           $(".subitem1kcm").css('background-color', '#FFF');
          $(".subitem1kcm").css('color', 'black');
          }
           else if(param==3)
          {
          $(".subitem1jmt").css('background-color', '#FB4602');
          $(".subitem1jmt").css('color', '#FFF');
          $(".subitem1twn").css('background-color', '#FFF');
          $(".subitem1twn").css('color', 'black');
           $(".subitem1spw").css('background-color', '#FFF');
          $(".subitem1spw").css('color', 'black');
           $(".subitem1kcm").css('background-color', '#FFF');
          $(".subitem1kcm").css('color', 'black');
          }
          else if(param==4)
          {
          $(".subitem1kcm").css('background-color', '#FB4602');
          $(".subitem1kcm").css('color', '#FFF');
          $(".subitem1twn").css('background-color', '#FFF');
          $(".subitem1twn").css('color', 'black');
           $(".subitem1spw").css('background-color', '#FFF');
          $(".subitem1spw").css('color', 'black');
           $(".subitem1jmt").css('background-color', '#FFF');
          $(".subitem1jmt").css('color', 'black');
          }
              //$("#myDiv").show();
           },
        success: function(response){




            $("#myDiv").hide();
            var len = response.length;
            for(var i=0; i<len; i++){

 /*               var id = response[i].eclast_code;
                var username = response[i].name;
                var name = response[i].main_image;
                var email = "email";*/
                var main_image=response[i].main_image;
                var id=response[i].id;
                var eclast_code=response[i].eclast_code;
                var eclast_price=response[i].eclast_price;
                var id_kategori=response[i].id_category;
                var category_slug=response[i].cat_slug;

                var merek_slug=response[i].merek_slug;
                var folder_x=category_slug.toUpperCase()+'-'+merek_slug.toUpperCase();

             
                var name=response[i].name;
                var slug=response[i].slug; var id_category=response[i].id_category;
                var folder_img;

                var url_cat;
                if(id_category == 2)
                {
                  folder_image="sepatu-wanita";
                  url_cat ='/produk/2-sepatu-wanita-bata.html';

                }
                else if(id_category == 1)
                {
                  folder_image="tas-wanita";
                  url_cat ='/produk/1-tas-wanita-sophie-martin.html';
                }
                  else if(id_category == 3)
                {
                  folder_image="jam-tangan";
                  url_cat ='/produk/3-jam-tangan-garmin.html';
                }
                 else if(id_category == 4)
                {
                  folder_image="kaca-mata";
                  url_cat ='/produk/4-kaca-mata-owl.html';
                }
                else
                {
                  folder_image=folder_x;
               //   url_cat ='/produk/'+id_kategori+'-'kaca-mata-owl'.html';
                  //alert("masuk sini");
                }

                //alert(id_category);
                var potongName=name.substring(0, 23);
                        var number_string = eclast_price.toString(),sisa  = number_string.length % 3,rupiah   = number_string.substr(0, sisa),ribuan  = number_string.substr(sisa).match(/\d{3}/g);
if (ribuan) {
  separator = sisa ? '.' : '';
  rupiah += separator + ribuan.join('.');
}
var hargaCoret = eclast_price*120/100;
            /*    var tr_str = "<tr>" +
                    "<td align='center'>" + (i+1) + "</td>" +
                    "<td align='center'>" + id + "</td>" +
                    "<td align='center'><img src='/storage/IMG/" +name + ".jpg'/></td>" +
                    "<td align='center'>" + username + "</td>" +
                    "</tr>";
            */   
             var tr_str='<div class="toRemove"><div class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>Diskon <br>18%</h6></div><a href="single.html">'+'<img src="/storage/'+folder_image+'/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><h5 style="font-family:calibri;"><a alt="data "href="/detail/'+id+'-'+slug+'.html">'+potongName+'...</a></h5><h6><small><i><del>Rp '+hargaCoret+'</del></i></small><b>Rp '+rupiah+'</b></h6><form action="#" method="post"><input type="hidden" name="cmd" value="_cart" /><input type="hidden" name="add" value="1" /> <input type="hidden" name="w3ls_item" value="Leather Jacket" /><input type="hidden" name="amount" value="20.00" /><button class="w3ls-cart pw3ls-cart"><i class="fa fa-check-square-o" aria-hidden="true"></i><a style="color:inherit" href="/detail/'+id+'-'+slug+'.html"> Lihat Detail</a></button> </form></div></div></div>';
             //alert(slug);

               // $("#produkTampung").append(tr_str);
                $("#produkTampung").append(tr_str);
/*                window.history.pushState("object or string", "Title", "/products/3-sepatuhk.html");*/
            }


         }

        });
    });
         function kategoriProduct(param,slugx)
         {
          /*alert($slugx);
          return;*/
   
                       $.LoadingOverlay("show");

          $('.cekbok').prop('checked',false);
         // NProgress.configure({ showSpinner: false });
          //NProgress.set(0);
          //NProgress.start();
          //NProgress.set(0.1);
          var url='/product/'+slugx;

         var matches = url.match(/uct\/\d.?-(.*).html/);
         var untukClass=matches[1].replace(/\//g,'-');
/*
         alert(untukClass);
         alert(folder_image);*/

         var cariMereknyaAja = url.match(/uct\/\d.*?\/(.*).html/);

         var cariKategorinyaAja = url.match(/uct\/\d*?-(.*?)\//);

         //alert(cariKategorinyaAja[1]);


         
        
         //var judulHeader =cariMereknyaAja[1].replace(/-/g,' ');
          //alert(cariMereknyaAja[1]);
        //  return;





         //tempSelect=cariMereknyaAja[1];




         

         var getToFolder=matches[1].replace(/\//g, '-').toUpperCase();
         //alert(folder_image);
       

         var judul;
         var tempSelect;
        // judul = folder_image.replace(/-/g, ' ');
        //alert(cariKategorinyaAja[1]);
        /*

        if(cariKategorinyaAja[1]=="jam-tangan")
        {
          tempSelect =folder_image.replace(/JAM-TANGAN-/g,''); 
          alert("masuk sini")
        }
        else if(cariKategorinyaAja[1]=="sepatu-dan-sandal")
        {
          tempSelect =folder_image.replace(/SEPATU-DAN-SANDAL-/g,''); 
        }*/




          var tempSelect2 =folder_image.replace(/JAM-TANGAN-/g,'jam-tangan-');
        
        /*  if(tempSelect2=TRUE)
          {
            alert("jam tangan");
          }*/
          tempSelect =tempSelect2.replace(/SEPATU-DAN-SANDAL-/g,'sepatu-dan-sandal-');
          /*if(tempSelect=TRUE)
          {
            alert("sepatu sendal");
          }

*/

       /* var n = folder_image.indexOf("JAM");
       // var n = folder_image.indexOf("S");
        alert(folder_image);
        if(n > 0)
        {
          tempSelect =folder_image.replace(/JAM-TANGAN-/g,'');
        }
        else 
        {
          tempSelect =folder_image.replace(/SEPATU-DAN-SANDAL-/g,'');
        }*/

       // alert(tempSelect);
        // RETURN;
         
 
/*
              alert(tempSelect.toLowerCase());
         return;*/


        
             
      
          $.ajax({

        url: "<?php echo site_url('Product/kategoriProduct/')?>"+param,
        type: "get",
        dataType: "JSON",
         beforeSend: function() {



   
          if(param==1)
          {
          $(".subitem1twn").css('background-color', '#FB4602');
          $(".subitem1twn").css('color', '#FFF');
          $(".subitem1spw").css('background-color', '#FFF');
          $(".subitem1spw").css('color', 'black');
           $(".subitem1jmt").css('background-color', '#FFF');
          $(".subitem1jmt").css('color', 'black');
           $(".subitem1kcm").css('background-color', '#FFF');
          $(".subitem1kcm").css('color', 'black');
          }
          else if(param==2)
          {
          $(".subitem1spw").css('background-color', '#FB4602');
          $(".subitem1spw").css('color', '#FFF');
          $(".subitem1twn").css('background-color', '#FFF');
          $(".subitem1twn").css('color', 'black');
           $(".subitem1jmt").css('background-color', '#FFF');
          $(".subitem1jmt").css('color', 'black');
           $(".subitem1kcm").css('background-color', '#FFF');
          $(".subitem1kcm").css('color', 'black');
          }
           else if(param==3)
          {
          $(".subitem1jmt").css('background-color', '#FB4602');
          $(".subitem1jmt").css('color', '#FFF');
          $(".subitem1twn").css('background-color', '#FFF');
          $(".subitem1twn").css('color', 'black');
           $(".subitem1spw").css('background-color', '#FFF');
          $(".subitem1spw").css('color', 'black');
           $(".subitem1kcm").css('background-color', '#FFF');
          $(".subitem1kcm").css('color', 'black');
          }
          else if(param==4)
          {
          $(".subitem1kcm").css('background-color', '#FB4602');
          $(".subitem1kcm").css('color', '#FFF');
          $(".subitem1twn").css('background-color', '#FFF');
          $(".subitem1twn").css('color', 'black');
           $(".subitem1spw").css('background-color', '#FFF');
          $(".subitem1spw").css('color', 'black');
           $(".subitem1jmt").css('background-color', '#FFF');
          $(".subitem1jmt").css('color', 'black');
          }
              //$("#myDiv").show();
      
           },
        success: function(response){

          var kelas="."+untukClass;
         // alert(kelas);
          $(kelas).css('background-color', '#FB4602');
          $(kelas).css('color', '#FFF');
         // alert(tempSelect);
         // return;
         var tl ="."+tempSelect.toLowerCase();
         tl.toString();

       //  alert(tl);
   


          $(tl).css('background-color', '#FFF');
          $(tl).css('color', 'black');


                    
    
          $(".toRemove").remove();
          //NProgress.done();
            //alert("it works");
            //$("#myDiv").hide();
               window.history.pushState(null, null, url);


            var len = response.length;
            for(var i=0; i<len; i++){
 /*               var id = response[i].eclast_code;
                var username = response[i].name;
                var name = response[i].main_image;
                var email = "email";*/
                var main_image=response[i].main_image;
                var id=response[i].id;
                var eclast_code=response[i].eclast_code;
                var eclast_price=response[i].eclast_price;
                var name=response[i].name;
                var slug=response[i].slug;
                var id_category=response[i].id_category;
                var folder_img;
                var url_cat;


                  folder_image=getToFolder;
                  url_cat =url;
                

                //alert(id_category);
                            var potongName=name.substring(0, 23);
                        var number_string = eclast_price.toString(),sisa  = number_string.length % 3,rupiah   = number_string.substr(0, sisa),ribuan  = number_string.substr(sisa).match(/\d{3}/g);
    
if (ribuan) {
  separator = sisa ? '.' : '';
  rupiah += separator + ribuan.join('.');
}

var hargaCoret = eclast_price*120/100;


            /*    var tr_str = "<tr>" +
                    "<td align='center'>" + (i+1) + "</td>" +
                    "<td align='center'>" + id + "</td>" +
                    "<td align='center'><img src='/storage/IMG/" +name + ".jpg'/></td>" +
                    "<td align='center'>" + username + "</td>" +
                    "</tr>";
            */   
          //   var tr_str='<div class="toRemove"><div class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>18%<br>Off</h6></div><a href="single.html">'+'<img src="/storage/'+folder_image+'/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><h5><a href="/detail/'+id+'-'+slug+'.html">'+potongName+'</a></h5><h6><del>$25</del>Rp '+rupiah+'</h6><form action="#" method="post"><input type="hidden" name="cmd" value="_cart" /><input type="hidden" name="add" value="1" /> <input type="hidden" name="w3ls_item" value="Leather Jacket" /><input type="hidden" name="amount" value="20.00" /><button class="w3ls-cart pw3ls-cart"><i class="fa fa-check-square-o" aria-hidden="true"></i><a style="color:inherit" href="/detail/'+id+'-'+slug+'.html"> Lihat Detail</a></button> </form></div></div></div>';



                var tr_str='<div class="toRemove"><div class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>18%<br>Off</h6></div><a href="single.html">'+'<img src="/storage/'+folder_image+'/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><h5 style="font-family:calibri;"><a data-toggle="tooltip" data-placement="left" title="'+name+'" alt="data "href="/detail/'+id+'-'+slug+'.html">'+potongName+'...</a></h5><h6><small><del>Rp'+hargaCoret+'</del></small><b>Rp '+rupiah+'</b></h6><form action="#" method="post"><input type="hidden" name="cmd" value="_cart" /><input type="hidden" name="add" value="1" /> <input type="hidden" name="w3ls_item" value="Leather Jacket" /><input type="hidden" name="amount" value="20.00" /><button class="w3ls-cart pw3ls-cart"><i class="fa fa-check-square-o" aria-hidden="true"></i><a style="color:inherit" href="/detail/'+id+'-'+slug+'.html"> Lihat Detail</a></button> </form></div></div></div>';
        

               // $("#produkTampung").append(tr_str);
                $("#produkTampung").append(tr_str);
                





                
            }
            $('#categoryName').text(cariMereknyaAja[1].replace(/-/g, ' ').toUpperCase()+" ( Ready "+len+" Varian )");

            $.LoadingOverlay("hide");



         }

        });
          //alert("it works");

         }

   
         function filterByName()
         {
          
          //$("#produkTampung").hide();
          
          //alert("test");
          //alert("filterByName");
      NProgress.configure({ showSpinner: false });
          //NProgress.set(0);
          NProgress.start();
          NProgress.set(0.1);
      
          
         
              $.ajax({

        url: "<?php echo site_url('Product/jsonData2')?>",
        type: "get",
        dataType: "JSON",
         beforeSend: function() {
         
          NProgress.set(0.5);
          
              //$("#myDiv").show();
              
           },
        success: function(response){
          NProgress.done();


          
            //alert("it works");
            //$("#myDiv").hide();
            $(".toRemove").remove();

            var len = response.length;
            for(var i=0; i<len; i++){
 /*               var id = response[i].eclast_code;
                var username = response[i].name;
                var name = response[i].main_image;
                var email = "email";*/
                var main_image=response[i].main_image;
                var id=response[i].id;
                var eclast_code=response[i].name;
                var eclast_price=response[i].eclast_price;
                var slug=response[i].slug;

                var number_string = eclast_price.toString(),sisa  = number_string.length % 3,rupiah   = number_string.substr(0, sisa),ribuan  = number_string.substr(sisa).match(/\d{3}/g);
    
if (ribuan) {
  separator = sisa ? '.' : '';
  rupiah += separator + ribuan.join('.');
}


            /*    var tr_str = "<tr>" +
                    "<td align='center'>" + (i+1) + "</td>" +
                    "<td align='center'>" + id + "</td>" +
                    "<td align='center'><img src='/storage/IMG/" +name + ".jpg'/></td>" +
                    "<td align='center'>" + username + "</td>" +
                    "</tr>";
            */    var tr_str='<div class="toRemove"><div  class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>18%<br>Off</h6></div><a href="single.html">'+'<img src="/storage/IMG/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><p><a href="/detail/'+id+'-'+slug+'.html">'+eclast_code+'</a></p><h6><del>$25</del>Rp '+rupiah+'</h6><form action="#" method="post"><input type="hidden" name="cmd" value="_cart" /><input type="hidden" name="add" value="1" /> <input type="hidden" name="w3ls_item" value="Leather Jacket" /><input type="hidden" name="amount" value="20.00" /><button class="w3ls-cart pw3ls-cart"><i class="fa fa-check-square-o" aria-hidden="true"></i><a style="color:inherit" href="/detail/'+id+'-'+slug+'.html"> Lihat Detail</a></button></form> </div></div></div>';
        
                $("#myDiv").hide();
                

                $("#produkTampung").append(tr_str);
          window.history.pushState("object or string", "Title", "/produk/3-sepatu.html?filter-by=popular");
            }
         

         }

        });
         }

         function filterByName2()
         {
          //$("#produkTampung").hide();
          
          //alert("test");
          //alert("filterByName");
              $.ajax({

        url: "<?php echo site_url('Product/jsonData')?>",
        type: "get",
        dataType: "JSON",
         beforeSend: function() {
              //$("#myDiv").show();

           },
        success: function(response){
          $(".toRemove").remove();
            //alert("it works");
            //$("#myDiv").hide();

            var len = response.length;
            for(var i=0; i<len; i++){
 /*               var id = response[i].eclast_code;
                var username = response[i].name;
                var name = response[i].main_image;
                var email = "email";*/
                var main_image=response[i].main_image;
                var id=response[i].id;
                var eclast_code=response[i].name;
                var eclast_price=response[i].eclast_price;
                var slug=response[i].slug;

                var number_string = eclast_price.toString(),sisa  = number_string.length % 3,rupiah   = number_string.substr(0, sisa),ribuan  = number_string.substr(sisa).match(/\d{3}/g);
    
if (ribuan) {
  separator = sisa ? '.' : '';
  rupiah += separator + ribuan.join('.');
}
            /*    var tr_str = "<tr>" +
                    "<td align='center'>" + (i+1) + "</td>" +
                    "<td align='center'>" + id + "</td>" +
                    "<td align='center'><img src='/storage/IMG/" +name + ".jpg'/></td>" +
                    "<td align='center'>" + username + "</td>" +
                    "</tr>";
            */    var tr_str='<div class="toRemove"><div class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>18%<br>Off</h6></div><a href="single.html">'+'<img src="/storage/IMG/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><p><a href="/detail/'+id+'-'+slug+'.html">'+eclast_code+'</a></p><h6><del>$25</del>Rp '+rupiah+'</h6></div></div></div>';
                $("#myDiv").hide();
                $("#produkTampung").append(tr_str);
          window.history.pushState("object or string", "Title", "/produk/3-handphone.html?filter-by=harga");
            }
         }
        });
         }
</script>
        <div class="products-row">
                      <div id="myDiv" >
         <!-- <img id="loading-image" src="/storage/SYSTEM_IMG/Eclipse.gif"/> --> 
        <br/><br/>
        <div class="marquee">
        <center><h3> Tunggu bentar ya kak, </h3><br/> <img src="http://hotel.befreetour.com/storage/IMG-HOTEL/transparent.gif"/></center></div>
    </div>
          <div id="produkTampung">
          </div>
          <div id="produkTampung2">
          </div>
            
          <div class="clearfix"> </div>
        

        </div>
        <!-- add-products --> 
<!--        <div class="w3ls-add-grids w3agile-add-products">
          <a href="#"> 
            <h4>TOP 10 TRENDS FOR YOU FLAT <span>20%</span> OFF</h4>
            <h6>Shop now <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></h6>
          </a>
        </div>  -->
        <!-- //add-products -->
      </div>
      <script>
    $(document).ready(function() {
                         $.LoadingOverlaySetup({
    color           : "rgba(255, 255, 255, 0.9)",
    image           : "/storage/SYSTEM_IMG/wait.gif",
    maxSize         : "200px",
    minSize         : "20px",
    resizeInterval  : 0,
    size            : "50%"
});


        $(".pilihWarna").click(function(){

            $.LoadingOverlay("show");

          var url      = window.location.href;

         var matches = url.match(/uct\/\d.?-(.*).html/);
 
         var getToFolder=matches[1].replace(/\//g, '-').toUpperCase();
          var cariMereknyaAja = url.match(/uct\/\d.*?\/(.*).html/);


            var pilih = [];
            var q;
            var param ;
            $.each($("input[name='warna']:checked"), function(){         
                pilih.push($(this).val());
            });
            param=pilih.join("_");

         var arr=url.split('/');
          var arr2=arr[4].split('-');

          var kategoriUrl=arr2[0];
         
            
           if(param=="")
           {

             var url="<?php echo site_url('Product/jsonData/')?>"+kategoriUrl;
       
           }
           else
           {
             var url="<?php echo site_url('Product/sort_byWarna/')?>"+kategoriUrl+"/"+param;
           }
       

             $.ajax({

        url: url,
        type: "get",
        dataType: "JSON",
         beforeSend: function() {
              
      
       
            
           },
        success: function(response){

          var filtered = response.filter(function(item) { 
            return item.id_category == kategoriUrl;

          });

       

           
            $(".toRemove").remove();
            NProgress.done();

            var len = filtered.length;

           // alert(len);
            
            if(len==0)
            {
                  $("#produkTampung").append("<div class='toRemove'><br/><center><h3> Kisaran harga segitu kosong kak,-_- </h3><br/><h5>Coba cari harga lain yach </h5><br/>. . .</center></div>"); 
                  return;
            }
            for(var i=0; i<len; i++){

             /* var xxx=response[i].paramX;
              alert(xxx);
          */      var main_image=filtered[i].main_image;
                var id=filtered[i].id;
                var eclast_code=filtered[i].eclast_code;
                var eclast_price=filtered[i].eclast_price;
                var name=filtered[i].name;
                var slug=filtered[i].slug;
                var id_category=response[i].id_category;
                
               var potongName=name.substring(0, 13);
                        var number_string = eclast_price.toString(),sisa  = number_string.length % 3,rupiah   = number_string.substr(0, sisa),ribuan  = number_string.substr(sisa).match(/\d{3}/g);
    
if (ribuan) {
  separator = sisa ? '.' : '';
  rupiah += separator + ribuan.join('.');
}

var hargaCoret = eclast_price*120/100;


            /*    var tr_str = "<tr>" +
                    "<td align='center'>" + (i+1) + "</td>" +
                    "<td align='center'>" + id + "</td>" +
                    "<td align='center'><img src='/storage/IMG/" +name + ".jpg'/></td>" +
                    "<td align='center'>" + username + "</td>" +
                    "</tr>";
            */   
             var tr_str='<div class="toRemove"><div class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>18%<br>Off</h6></div><a href="single.html">'+'<img src="/storage/'+folder_image+'/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><h5 style="font-family:calibri;"><a alt="data "href="/detail/'+id+'-'+slug+'.html">'+potongName+'...</a></h5><h6><small><del>Rp'+hargaCoret+'</del></small><b>Rp '+rupiah+'</b></h6><form action="#" method="post"><input type="hidden" name="cmd" value="_cart" /><input type="hidden" name="add" value="1" /> <input type="hidden" name="w3ls_item" value="Leather Jacket" /><input type="hidden" name="amount" value="20.00" /><button class="w3ls-cart pw3ls-cart"><i class="fa fa-check-square-o" aria-hidden="true"></i><a style="color:inherit" href="/detail/'+id+'-'+slug+'.html"> Lihat Detail</a></button> </form></div></div></div>';

                $("#produkTampung").append(tr_str);

            }

                         $('#categoryName').text(cariMereknyaAja[1].replace(/-/g, ' ').toUpperCase()+" ( Ready "+len+" Varian )");

            $.LoadingOverlay("hide");



         }
    });



          });

        $(".cekbok").click(function(){
           //$(".toRemove").remove();
                                  $.LoadingOverlay("show");


         var url      = window.location.href;

         var matches = url.match(/uct\/\d.?-(.*).html/);
 
         var getToFolder=matches[1].replace(/\//g, '-').toUpperCase();

         //alert(getToFolder);
         //return;

          var arr=url.split('/');
          var arr2=arr[4].split('-');

          var kategoriUrl=arr2[0];
          /*alert(kategoriUrl);
          return;*/
          //alert(arr2[0]);
        /*
          NProgress.configure({ showSpinner: false });
          //NProgress.set(0);
          NProgress.start();
          NProgress.set(0.1);*/
            var pilih = [];
            var q;
            var param ;
            $.each($("input[name='harga']:checked"), function(){         
                pilih.push($(this).val());
            });
            param=pilih.join("_");
          // alert(param);
           if(param=="")
           {

             var url="<?php echo site_url('Product/jsonData/')?>"+kategoriUrl;
       
           }
           else
           {
             var url="<?php echo site_url('Product/sort_byPrice/')?>"+kategoriUrl+"/"+param;
           }
            //return;
            //alert("Anda memilih range harga: " +param );
            
          //alert(url);

             $.ajax({

        url: url,
        type: "get",
        dataType: "JSON",
         beforeSend: function() {
      
          //alert("sukses");
            
           },
        success: function(response){

          var filtered = response.filter(function(item) { 
            return item.id_category == kategoriUrl;

          });

          //alert(kategoriUrl);



           
            $(".toRemove").remove();
           // NProgress.done();

            var len = filtered.length;
            
            if(len==0)
            {
                  $("#produkTampung").append("<div class='toRemove'><br/><center><h3> Kisaran harga segitu kosong kak, -_-  </h3><br/><h5>Coba cari harga lain yach </h5><br/>. . .</center></div>"); 
                  
                                   $.LoadingOverlay("hide");
                  return;
            }
            for(var i=0; i<len; i++){
             /* var xxx=response[i].paramX;
              alert(xxx);
          */      var main_image=filtered[i].main_image;
                var id=filtered[i].id;
                var eclast_code=filtered[i].eclast_code;
                var eclast_price=filtered[i].eclast_price;
                var name=filtered[i].name;
                var slug=filtered[i].slug;
                var id_category=response[i].id_category;
              /*  var potongName=name.substring(0, 35);
                        var number_string = eclast_price.toString(),sisa  = number_string.length % 3,rupiah   = number_string.substr(0, sisa),ribuan  = number_string.substr(sisa).match(/\d{3}/g);
    
if (ribuan) {
  separator = sisa ? '.' : '';
  rupiah += separator + ribuan.join('.');
}*/
/*if(id_category == 2)
                {
                  folder_image="sepatu-wanita";
                  url_cat ='/produk/2-sepatu-wanita-bata.html';

                }
                else if(id_category == 1)
                {
                  folder_image="tas-wanita";
                  url_cat ='/produk/1-tas-wanita-sophie-martin.html';
                }
                  else if(id_category == 3)
                {
                  folder_image="jam-tangan";
                  url_cat ='/produk/3-jam-tangan-garmin.html';
                }
                 else if(id_category == 4)
                {
                  folder_image="kaca-mata";
                  url_cat ='/produk/4-kaca-mata-owl.html';
                }
                else
                {*/

                folder_image=getToFolder;
                //  alert(folder_image);

              

              //  alert(id_category);


            /* var tr_str='<div class="toRemove"><div class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>18%<br>Off</h6></div><a href="single.html">'+'<img src="/storage/'+folder_image+'/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><h5><a href="/detail/'+id+'-'+slug+'.html">'+potongName+'</a></h5><h6><del>'+(rupiah*120)/100+'0</del>Rp '+rupiah+'</h6></div></div></div>';*/
/*              var tr_str='<div class="toRemove"><div class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>18%<br>Off</h6></div><a href="single.html">'+'<img src="/storage/'+folder_image+'/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><h5 ><a href="/detail/'+id+'-'+slug+'.html">'+potongName+'</a></h5><h6><del>$25</del>Rp '+rupiah+'</h6><form action="#" method="post"><input type="hidden" name="cmd" value="_cart" /><input type="hidden" name="add" value="1" /> <input type="hidden" name="w3ls_item" value="Leather Jacket" /><input type="hidden" name="amount" value="20.00" /><button class="w3ls-cart pw3ls-cart"><i class="fa fa-check-square-o" aria-hidden="true"></i><a style="color:inherit" href="/detail/'+id+'-'+slug+'.html"> Lihat Detail</a></button> </form></div></div></div>';*/

               var potongName=name.substring(0, 13);
                        var number_string = eclast_price.toString(),sisa  = number_string.length % 3,rupiah   = number_string.substr(0, sisa),ribuan  = number_string.substr(sisa).match(/\d{3}/g);
    
if (ribuan) {
  separator = sisa ? '.' : '';
  rupiah += separator + ribuan.join('.');
}

var hargaCoret = eclast_price*120/100;


            /*    var tr_str = "<tr>" +
                    "<td align='center'>" + (i+1) + "</td>" +
                    "<td align='center'>" + id + "</td>" +
                    "<td align='center'><img src='/storage/IMG/" +name + ".jpg'/></td>" +
                    "<td align='center'>" + username + "</td>" +
                    "</tr>";
            */   
             var tr_str='<div class="toRemove"><div class="col-md-3 product-grids"> <div class="agile-products">'+'<div class="new-tag"><h6>18%<br>Off</h6></div><a href="single.html">'+'<img src="/storage/'+folder_image+'/'+ main_image +'.jpg" class="img-responsive" alt="img"/></a><div class="agile-product-text"><h5 style="font-family:calibri;"><a alt="data "href="/detail/'+id+'-'+slug+'.html">'+potongName+'...</a></h5><h6><small><del>Rp'+hargaCoret+'</del></small><b>Rp '+rupiah+'</b></h6><form action="#" method="post"><input type="hidden" name="cmd" value="_cart" /><input type="hidden" name="add" value="1" /> <input type="hidden" name="w3ls_item" value="Leather Jacket" /><input type="hidden" name="amount" value="20.00" /><button class="w3ls-cart pw3ls-cart"><i class="fa fa-check-square-o" aria-hidden="true"></i><a style="color:inherit" href="/detail/'+id+'-'+slug+'.html"> Lihat Detail</a></button> </form></div></div></div>';

        if(tr_str=="")
        {
          alert("oops data kosong");
        }
                $("#produkTampung").append(tr_str);
/*                window.history.pushState("object or string", "Title", "/products/3-sepatuh.html");*/
            }

                                   $.LoadingOverlay("hide");


         }
    });



        });
        });
      </script>
      <div class="col-md-3 rsidebar">
        <div class="rsidebar-top">
          <div class="slider-left">
            <h4>Cari Harga</h4>            
            <div class="row row1 scroll-pane">
              <label class="checkbox"><input type="checkbox" value="0-50000" name="harga" class="cekbok"><i></i>< 50.000 </label>
              <label class="checkbox"><input type="checkbox" name="harga" value="50000-75000"class="cekbok" ><i></i>50.000 - 75.000 </label> 
              <label class="checkbox"><input type="checkbox" name="harga" value="75000-125000"class="cekbok"><i></i>75.000 - 125.000  </label> 
              <label class="checkbox"><input type="checkbox" name="harga" value="125000-200000"class="cekbok"><i></i>125.000 - 200.000 </label> 
              <label class="checkbox"><input type="checkbox" name="harga" value="200000-300000"class="cekbok"><i></i>200.000 - 300.000 </label> 
                          </div> 
          </div>
          <div class="sidebar-row">
            <h4>Cari Warna</h4>
            <div class="row row1 scroll-pane">
              <label class="checkbox"><input type="checkbox" name="warna" class="pilihWarna" value="black"><i></i>Hitam</label>
              <label class="checkbox"><input type="checkbox" name="warna" class="pilihWarna" value="white"><i></i>Putih</label>
              <label class="checkbox"><input type="checkbox" name="warna" class="pilihWarna" value="blue"><i></i>Biru</label>
              <label class="checkbox"><input type="checkbox" name="warna" class="pilihWarna" value="red"><i></i>Merah</label>
              <label class="checkbox"><input type="checkbox" name="warna" class="pilihWarna" value="green"><i></i>Hijau</label>
              <label class="checkbox"><input type="checkbox" name="warna" class="pilihWarna" value="brown"><i></i>Coklat</label> 
            </div>
          </div>
          <div class="sidebar-row">
            <h4>Kategori</h4>
            <ul class="faq">
              <li class="item1"><a href="#"><b>Tas Wanita (<?=$jumlahKategoriTasWanita;?>)<span class="glyphicon glyphicon-menu-down"></span></a>
                <ul>

                  <?php
                  //$slugKategori="";


                   foreach ($kategoriTasWanita->result() as $baris)
                  {
                     $slugKategori=$baris->id."-".$baris->cat_slug."/".$baris->merek_slug.".html";

                  ?>
                  <li class="<?=$baris->merek_slug;?>"><a  class="<?=$baris->cat_slug.'-'.$baris->merek_slug;?>" href="#top-merch"  onclick = "return kategoriProduct(<?=$baris->id;?>,'<?=$slugKategori;?>')"><small><?php echo $baris->merek;//$slugKategori//$baris->merek;?></small></a></li>
                  <?php
                  }

                  ?>  

                </ul>
              </li>
              <li class="item2"><a href="#"><b>Jam Tangan (<?=$jumlahKategoriJam;?>)</b><span class="glyphicon glyphicon-menu-down"></span></a>
                <ul>


                  <?php
                  //$slugKategori="";


                   foreach ($kategoriJam->result() as $baris)
                  {
                     $slugKategori=$baris->id."-".$baris->cat_slug."/".$baris->merek_slug.".html";

                  ?>
                  <li class="<?=$baris->merek_slug;?>"><a  class="<?=$baris->cat_slug.'-'.$baris->merek_slug;?>" href="#top-merch"  onclick = "return kategoriProduct(<?=$baris->id;?>,'<?=$slugKategori;?>')"><small><?php echo $baris->merek;//$slugKategori//$baris->merek;?></small></a></li>
                  <?php
                  }

                  ?>  


                  <li class="subitem1jmt"><a  class="subitem1jmt" href="#targetz"  onclick = "return kategoriProduct(3)">Garmin</a></li>                      
                </ul>
              </li>
              <li class="item3"><a href="#"><b>Sepatu & Sandal (<?=$jumlahKategoriSepatu;?>)</b><span class="glyphicon glyphicon-menu-down"></span></a>
                <ul>

                  <?php
                  //$slugKategori="";


                   foreach ($kategoriSepatu->result() as $baris)
                  {
                     $slugKategori=$baris->id."-".$baris->cat_slug."/".$baris->merek_slug.".html";

                  ?>
                  <li class="<?=$baris->merek_slug;?>"><a  class="<?=$baris->cat_slug.'-'.$baris->merek_slug;?>" href="#top-merch"  onclick = "return kategoriProduct(<?=$baris->id;?>,'<?=$slugKategori;?>')"><small><?php echo $baris->merek;//$slugKategori//$baris->merek;?></small></a></li>
                  <?php
                  }

                  ?>  

                </ul>
              </li>

                <li class="item3"><a href="#">Kaca Mata<span class="glyphicon glyphicon-menu-down"></span></a>
                <ul>
                  <li class="subitem1kcm"><a class="subitem1kcm" href="#targetz" onclick = "return kategoriProduct(4)">OWL</a></li>
                  <li class="subitem1"><a href="#">Adidas</a></li>                            
                </ul>
              </li>
            </ul>
            <!-- script for tabs -->
            <script type="text/javascript">
              $(function() {
              
                var menu_ul = $('.faq > li > ul'),
                     menu_a  = $('.faq > li > a');
                
                menu_ul.show();
              
                menu_a.click(function(e) {
                  e.preventDefault();
                  if(!$(this).hasClass('active')) {
                    menu_a.removeClass('active');
                    menu_ul.filter(':visible').slideUp('normal');
                    $(this).addClass('active').next().stop(true,true).slideDown('normal');
                  } else {
                    $(this).removeClass('active');
                    $(this).next().stop(true,true).slideUp('normal');
                  }
                });
              
              });
            </script>
            <!-- script for tabs -->
          </div>
          <div class="sidebar-row">
            <h4>DISCOUNTS</h4>
            <div class="row row1 scroll-pane">
              <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Upto - 10% (20)</label>
              <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>70% - 60% (5)</label>
              <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>50% - 40% (7)</label>
              <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>30% - 20% (2)</label>
              <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>10% - 5% (5)</label>
              <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>30% - 20% (7)</label>
              <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>10% - 5% (2)</label>
              <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Other(50)</label>
            </div>
          </div>
                 
        </div>
        <div class="related-row">
          <h4>Related Searches</h4>
          <ul>
            <li><a href="products.html">Travel Bags</a></li>
            <li><a href="products.html">Casual Wear</a></li>
            <li><a href="products.html">Beauty Gift Hampers</a></li>
            <li><a href="products.html">T-Shirts</a></li>
            <li><a href="products.html">Blazers</a></li>
            <li><a href="products.html">Parkas</a></li>
            <li><a href="products.html">Shoes</a></li>
            <li><a href="products.html">Hair Care</a></li>
            <li><a href="products.html">Bath & Spa</a></li>
            <li><a href="products.html">Handbags</a></li>
          </ul>
        </div>
        <div class="related-row">
          <h4>YOU MAY ALSO LIKE</h4>
          <div class="galry-like">  
            <a href="single.html"><img src="images/e1.png" class="img-responsive" alt="img"></a>             
            <h4><a href="products.html">Audio speaker</a></h4> 
            <h5>$100</h5>       
          </div>
        </div>
      </div>
      <div class="clearfix"> </div>
      <!-- recommendations -->
    
      <!-- //recommendations -->
    </div>
  </div>
  <!--//products-->  
  <!-- footer-top -->
  <div class="w3agile-ftr-top">
    <div class="container">
      <div class="ftr-toprow">
        <div class="col-md-4 ftr-top-grids">
          <div class="ftr-top-left">
            <i class="fa fa-truck" aria-hidden="true"></i>
          </div> 
          <div class="ftr-top-right">
            <h4>FREE DELIVERY</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
          </div> 
          <div class="clearfix"> </div>
        </div> 
        <div class="col-md-4 ftr-top-grids">
          <div class="ftr-top-left">
            <i class="fa fa-user" aria-hidden="true"></i>
          </div> 
          <div class="ftr-top-right">
            <h4>CUSTOMER CARE</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
          </div> 
          <div class="clearfix"> </div>
        </div>
        <div class="col-md-4 ftr-top-grids">
          <div class="ftr-top-left">
            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
          </div> 
          <div class="ftr-top-right">
            <h4>GOOD QUALITY</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
          </div>
          <div class="clearfix"> </div>
        </div> 
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>
  <!-- //footer-top --> 
  <!-- subscribe -->

  <!-- //subscribe --> 
  <!-- footer -->

  <!-- //footer -->
  <div class="copy-right" style="background-color:black;"> 
    
      <p>© 2018 Dzy
      Project&trade; | All rights reserved</p>
   
  </div> 
  
  <!-- cart-js -->
  <script src="/asset/js/minicart.js"></script>
  <script>
 
  </script>
  <script>
        w3ls.render();

        w3ls.cart.on('w3sb_checkout', function (evt) {
          var items, len, i;

          if (this.subtotal() > 0) {
            items = this.items();

            for (i = 0, len = items.length; i < len; i++) {
              items[i].set('shipping', 0);
              items[i].set('shipping2', 0);
            }
          }
        });
    </script>  
  <!-- //cart-js -->  
  <!-- menu js aim -->
  <script src="/asset/js/jquery.menu-aim.js"> </script>
  <script src="/asset/js/main.js"></script> <!-- Resource jQuery -->
  <!-- //menu js aim --> 
  <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/asset/js/bootstrap.js"></script>
</body>
</html>