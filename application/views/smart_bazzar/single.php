<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Eclast Store | New Look & Always Bold</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Smart Bazaar Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href='//fonts.googleapis.com/css?family=Risque' rel='stylesheet' type='text/css'>

<link href="/asset/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="/asset/css/style.css" rel="stylesheet" type="text/css" media="all" /> 
<link href="/asset/css/animate.min.css" rel="stylesheet" type="text/css" media="all" /><!-- animation -->
<link href="/asset/css/menu.css" rel="stylesheet" type="text/css" media="all" /> <!-- menu style -->   
<link href="/asset/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all"> <!-- carousel slider -->  
<!-- //Custom Theme files -->
<!-- font-awesome icons -->
<link href="/asset/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="/asset/js/jquery-2.2.3.min.js"></script> 
<script src="/asset/js/owl.carousel.js"></script>
<script src="/asset/js/bootstrap.js"></script>
<!--flex slider-->
<script defer src="/asset/js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="/asset/css/flexslider.css" type="text/css" media="screen" />
<script>
	// Can also be used with $(document).ready()
	$(window).load(function() {
	  $('.flexslider').flexslider({
		animation: "slide",
		controlNav: "thumbnails"
	  });
	});
</script>
<!--flex slider-->
<script src="/asset/js/imagezoom.js"></script>
<!-- //js --> 
<!-- web-fonts -->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lovers+Quarrel' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Offside' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Tangerine:400,700' rel='stylesheet' type='text/css'>
<!-- web-fonts --> 
<!-- scroll to fixed--> 
<script src="/asset/js/jquery-scrolltofixed-min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        // Dock the header to the top of the window when scrolled past the banner. This is the default behaviour.
        $('.header-two').scrollToFixed();  
        // previous summary up the page.
        var summaries = $('.summary');
        summaries.each(function(i) {
            var summary = $(summaries[i]);
            var next = summaries[i + 1];
            summary.scrollToFixed({
                marginTop: $('.header-two').outerHeight(true) + 10, 
                zIndex: 999
            });
        });
    });
</script>
<!-- //scroll to fixed--> 
<!-- start-smooth-scrolling -->
<script type="text/javascript" src="/asset/js/move-top.js"></script>
<script type="text/javascript" src="/asset/js/easing.js"></script>	
<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!-- //end-smooth-scrolling -->
<!-- smooth-scrolling-of-move-up -->
<script type="text/javascript">
	$(document).ready(function() {
	
		var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
		};
		
		$().UItoTop({ easingType: 'easeOutQuart' });
		
	});
</script>
<!-- //smooth-scrolling-of-move-up -->  
</head>
<body>
	<!-- header --> 
	
  <!-- header --><div class="header" >
    <div class="w3ls-header" style="background-color:black;"><!--header-one--> 
      <div class="w3ls-header-left">
        <p><a href="#">Jam kerja : Senin - Jumat | 08.00 - 21.00 </a></p>
      </div>
      <div class="w3ls-header-right">
        <ul>

         <li class="dropdown head-dpdn">
            <a href="help.html" class="dropdown-toggle"><i class="fa fa-facebook" aria-hidden="true"></i> eclaststore</a>
          </li> 
         <li class="dropdown head-dpdn">
            <a href="help.html" class="dropdown-toggle"><i class="fa fa-whatsapp" aria-hidden="true"></i> 081-385-800-400</a>
          </li> 

          <li class="dropdown head-dpdn">
            <a href="card.html" class="dropdown-toggle"><i class="fa fa-instagram" aria-hidden="true"></i> eclaststore</a>
          </li>
          <li class="dropdown head-dpdn">
            <a href="contact.html" class="dropdown-toggle"><i class="fa fa-envelope" aria-hidden="true"></i> bantuan@eclast-store.com</a>
          </li>  
         
        </ul>
      </div>


      <div class="clearfix"> </div> 
    </div>

    <div class="header-two"><!-- header-two -->
      <div class="container">
         <div class="header-logo">
          <h1><a href="/"><span style="">E</span>clast<i><sub>_store.com</sub></i></a></h1>
          <h6>New Look & Always Bold.</h6> 
        </div> 
        <div class="header-search" style="">
          <form action="#" method="post">
            <input type="search" name="Search" placeholder="Cari aja disini..." required="">
            <button type="submit" class="btn btn-default" aria-label="Left Align">
              <i class="fa fa-search" aria-hidden="true"> </i>
            </button>
          </form>
        </div>
        <div class="header-cart" style="">
          <div class="row">
         <div class="col-md-12">
           <div class="row">

          <div class="col-md-12">
            <center><h3> Customer Services</h3></center>
          </div>
          <div class="col-md-12" style="height:5px">
  
          </div>

          <div class="col-md-6">
           <center><h4 style="color:white;background-color:green"><i class="fa fa-whatsapp" aria-hidden="true" style="color:white;background-color:green"></i> 081 385 800 400</h4></center>
          </div>
          <div class="col-md-6">
            <center><h4 style="color:black;background-color:yellow"><i class="fa fa-wechat" aria-hidden="true" style="color:black;background-color:yellow"></i> @eclaststore</h4></center>
          </div>


        </div>



         </div>
       </div>
          <div class="clearfix" > </div> 
        </div> 

        <div class="clearfix"> </div>
      </div>    
    </div><!-- //header-two -->
    

                  <div id="top-merch"></div> 
<?php
include('include/menu.php');
?>
	<!-- //header --> 
	<!-- breadcrumbs --> 
	<div class="container"> 
		<ol class="breadcrumb breadcrumb1" style="background-color:white">
			<li><a href="/">Home</a></li><?php foreach($dataProduk->result() as $resulte)
{?>
			<li><a href="/product/<?=$resulte->id."-".$resulte->cat_slug."/".$resulte->merek_slug?>.html">
				<?php echo $resulte->cat_name." ".$resulte->merek ?></a></li>
			
			<li class="active">
				<?php
	echo $resulte->name;
}
?>
</li>
		</ol> 
		
	</div>
	<!-- //breadcrumbs -->
	<!-- products -->
<?php	
function rupiah($nilai, $pecahan = 0) {
    return number_format($nilai, $pecahan, ',', '.');
}
	foreach($dataProduk->result() as $result)
{
	function get_http_response_code($url) {
    $headers = get_headers($url);
    return substr($headers[0], 9, 3);
}

$folder=strtoupper($result->cat_slug."-".$result->merek_slug);


						for($i=1,$dataTerakhir=0;$i<=10;$i++){
								if (file_get_contents("storage/".$folder."/".$result->main_image."-".$i.".jpg") === false)
									{
										$dataTerakhir=$i-1;
										break;
									}
									else
									{
										echo "";
									}



								}
	/*							echo $folder;
								echo $dataTerakhir;
*/
     
?>
							
	<div class="products">	 
		<div class="container">  
			<div class="single-page">
				<div class="single-page-row" id="detail-21">
					<div class="col-md-6 single-top-left">	
						<div class="flexslider">
							<ul class="slides" >
								<?php
		
								for($i=1;$i<=$dataTerakhir;$i++)
								{ 

								?>

								<li data-thumb="/storage/<?=$folder;?>/<?=$result->main_image;?>-<?=$i?>.jpg">
									<div class="thumb-image detail_images" > <img src="/storage/<?=$folder;?>/<?=$result->main_image;?>-<?=$i?>.jpg" data-imagezoom="true" class="img-responsive"  alt="" > </div>
								</li>
								<?php
							}

								?>
							</ul>
						</div>
					</div>
					<div class="col-md-6 single-top-right">
						<h3 class="item_name" style="font-family:arial"><?=$result->name;?></h3>
						
						<div class="single-rating">
							<ul>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								
							</ul> 
						</div>
						<div class="single-price">
							<ul>
								<li>Rp <?php 
								echo rupiah($result->eclast_price,2);

								?></li>  
								<li><del><?php
								echo rupiah($result->eclast_price+87000,2);?></del></li> 
								<li><span class="w3off">Diskon 10% </span></li> 
								
							</ul>	
						</div> 
						<p class="single-price-text"><?=$result->description;?> </p>
						
							<input type="hidden" name="cmd" value="_cart" />
							<input type="hidden" name="add" value="1" /> 
							<input type="hidden" name="w3ls_item" value="<?=$result->name;?>" /> 
							<input type="hidden" name="amount" value="<?=$result->eclast_price+87000?>" /> 
							<?php
							$salt=rand(10,1000);
							$salt2=date("s");
							$encrypt_key=base64_encode($salt."#".$result->eclast_code."/".$salt2);
							$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
							if (strpos($encrypt_key, '=') == true) 
							{
								header("Location:".$actual_link);
								//$encrypt_key2="ketemu";								
/*								header("Location:".$actual_link);
								die();   */
								//continue;
						    }
						    else
						    {
						    	//header("Location:".$actual_link);
						    	$encrypt_key2=$encrypt_key;
								
								//$encrypt_key="kedua";
						    	//echo "fasle";
						    }
							?>
							<a href="<?=site_url();?>order.py/<?=$encrypt_key2;?>"><button  class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Beli</button></a>
						
						<button class="w3ls-cart w3ls-cart-like"><i class="fa fa-envelope-o" aria-hidden="true"></i> Chat </button>
						<img src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=https://api.whatsapp.com/send?phone=6289521084027&amp;size=120x120" alt="" title="" />
					</div>
				   <div class="clearfix"> </div>  
				</div>
				<div class="single-page-icons social-icons"> 
					<ul>
						<li><h4>Share on</h4></li>
						<li><a href="#" class="fa fa-facebook icon facebook"> </a></li>
						<li><a href="#" class="fa fa-twitter icon twitter"> </a></li>
						<li><a href="#" class="fa fa-google-plus icon googleplus"> </a></li>
						<li><a href="#" class="fa fa-dribbble icon dribbble"> </a></li>
						<li><a href="#" class="fa fa-rss icon rss"> </a></li> 
					</ul>
				</div>
			</div> 
			<?php
		}
			?>
			<!-- recommendations -->
			<div class="recommend">
				<h3 class="w3ls-title">Produk  serupa : </h3> 
				<script>
					$(document).ready(function() { 
						$("#owl-demo5").owlCarousel({
					 
						  autoPlay: 3000, //Set AutoPlay to 3 seconds
					 
						  items :4,
						  itemsDesktop : [640,5],
						  itemsDesktopSmall : [414,4],
						  navigation : true
					 
						});
						
					}); 
				</script>


			
				<div id="owl-demo5" class="owl-carousel">



					<?php foreach($dataProdukSerupa->result() as $resultnya)
{

$folder=strtoupper($resultnya->cat_slug."-".$resultnya->merek_slug);




	?>

					<div class="item">
						<div class="glry-w3agile-grids agileits">
							<div class="new-tag"><h6> <br> Off</h6></div>
							<a href="products1.html"><img src="/storage/<?=$folder;?>/<?=$resultnya->main_image;?>.jpg" alt="img"></a>
							<div class="view-caption agileits-w3layouts">           
								<h4 style="font-family:arial;font-size:11pt"><a href="products1.html"><?=$resultnya->name?></a></h4>

								<h5><?=$resultnya->eclast_price?></h5>
								<form action="#" method="post">
									<input type="hidden" name="cmd" value="_cart" />
									<input type="hidden" name="add" value="1" /> 
									<input type="hidden" name="w3ls_item" value="Women Sandal" /> 
									<input type="hidden" name="amount" value="20.00" /> 
									<button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button>
								</form>
							</div>        
						</div> 
					</div> 


<?php
}
?>
					 
				</div>    
			</div>
			<!-- //recommendations --> 
			<!-- collapse-tabs -->
		
			<!-- //offers-cards -->
		</div>
	</div>
	<!--//products-->  
	<!-- footer-top -->
	<div class="w3agile-ftr-top">
		<div class="container">
			<div class="ftr-toprow">
				<div class="col-md-4 ftr-top-grids">
					<div class="ftr-top-left">
						<i class="fa fa-truck" aria-hidden="true"></i>
					</div> 
					<div class="ftr-top-right">
						<h4>FREE DELIVERY</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
					</div> 
					<div class="clearfix"> </div>
				</div> 
				<div class="col-md-4 ftr-top-grids">
					<div class="ftr-top-left">
						<i class="fa fa-user" aria-hidden="true"></i>
					</div> 
					<div class="ftr-top-right">
						<h4>CUSTOMER CARE</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
					</div> 
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-4 ftr-top-grids">
					<div class="ftr-top-left">
						<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
					</div> 
					<div class="ftr-top-right">
						<h4>GOOD QUALITY</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
					</div>
					<div class="clearfix"> </div>
				</div> 
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //footer-top -->  
	<!-- subscribe -->
 
	<!-- //subscribe --> 
	<!-- footer -->
 
	<!-- //footer --> 		
	<div class="copy-right" style="background-color:black"> 
		<div class="container">
			<p>© 2018 <a href="http://www.minormood.work">Bluellespie Project</a> . All rights reserved </p>
		</div>
	</div> 
	<!-- cart-js -->
	<script src="/asset/js/minicart.js"></script>
	<script>
        w3ls.render();

        w3ls.cart.on('w3sb_checkout', function (evt) {
        	var items, len, i;

        	if (this.subtotal() > 0) {
        		items = this.items();

        		for (i = 0, len = items.length; i < len; i++) {
        			items[i].set('shipping', 0);
        			items[i].set('shipping2', 0);
        		}
        	}
        });
    </script>  
	<!-- //cart-js --> 	 
	<!-- menu js aim -->
	<script src="/asset/js/jquery.menu-aim.js"> </script>
	<script src="/asset/js/main.js"></script> <!-- Resource jQuery -->
	<!-- //menu js aim --> 
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster --> 
</body>
</html>