<!doctype html>
<html>
    <head>
        <title>Return JSON response from AJAX using jQuery and PHP</title>
        
        <script src="/asset/js/jquery-3.1.1.min.js" type="text/javascript"></script>

    </head>
    <body>
        <?php echo site_url('Product/jsonData')?>
        <script>
         $(document).ready(function(){
         //$('#userTable').html(' loading...');
             
    $.ajax({

        url: "<?php echo site_url('Product/jsonData')?>",
        type: "get",
        dataType: "JSON",
         beforeSend: function() {
              $("#myDiv").show();
           },
        success: function(response){
            //alert("it works");
            $("#myDiv").hide();

            var len = response.length;
            for(var i=0; i<len; i++){
                var id = response[i].eclast_code;
                var username = response[i].name;
                var name = response[i].main_image;
                var email = "email";

                var tr_str = "<tr>" +
                    "<td align='center'>" + (i+1) + "</td>" +
                    "<td align='center'>" + id + "</td>" +
                    "<td align='center'><img src='/storage/IMG/" +name + ".jpg'/></td>" +
                    "<td align='center'>" + username + "</td>" +
                    "</tr>";

                $("#userTable").append(tr_str);
            }


         }

        });
    });

        </script>

        <div class="container">


            <div id="myDiv">
         <img id="loading-image" src="/storage/SYSTEM_IMG/Eclipse.gif"/> 
        <p>Loading . . .</p>
    </div>
            <table id="userTable" border="1" >
                <tr>
                    <th width="5%">S.no</th>
                    <th width="20%">Username</th>
                    <th width="20%">Name</th>
                    <th width="30%">Email</th>
                </tr>
            </table>
        </div>
    </body>
</html>